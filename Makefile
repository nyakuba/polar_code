CC = icpc

SCD = src/successive_cancellation_decoder/
LD  = src/list_decoder/
SDS = src/sequential_decoder_set/
SDF = src/sequential_decoder_fibonacci_heap/
SDM = src/sequential_decoder_minmax_heap/

DEPS = main.cpp \
       encoding.cpp \
       code.cpp \
       channel.cpp \
       GaussianApproximation.cpp \
       decoder.cpp 

TEST_DEPS = test/main.cpp \
	    test/test_code.cpp \
	    test/test_encoding.cpp

FLAGS = -W -Wall -std=c++11
LIBS = -lm -lgsl -lgslcblas
OPT = -axSSE4.2 \
      -fast \
      -xHost \
      -use-intel-optimized-headers

debug: 
	make seq_fib_debug

release:
	make seq_fib_release

# rules for different decoder types
# sequential decoder with fibonacci heap
seq_fib_release: $(addprefix $(SDF), $(DEPS))
	$(CC) $(OPT) $(FLAGS) -I./$(SDF) $^ -o bin/pc $(LIBS) -DNO_LOGS

seq_fib_debug: $(addprefix $(SDF), $(DEPS))
	$(CC) -g $(FLAGS) -I./$(SDF) $^ -o bin/debug_pc $(LIBS)

# sequential decoder with minmax heap
seq_mm_release: $(addprefix $(SDM), $(DEPS))
	$(CC) $(OPT) $(FLAGS) -I./$(SDM) $^ -o bin/pc $(LIBS) -DNO_LOGS

seq_mm_debug: $(addprefix $(SDM), $(DEPS))
	$(CC) -g $(FLAGS) -I./$(SDM) $^ -o bin/debug_pc $(LIBS)

# sequential decoder with stl set
seq_set_release: $(addprefix $(SDS), $(DEPS))
	$(CC) $(OPT) $(FLAGS) -I./$(SDS) $^ -o bin/pc $(LIBS) -DNO_LOGS
seq_set_debug: $(addprefix $(SDS), $(DEPS))
	$(CC) -g $(FLAGS) -I./$(SDS) $^ -o bin/debug_pc $(LIBS)


# list decoder
list_release: $(addprefix $(LD), $(DEPS))
	$(CC) $(OPT) $(FLAGS) -I./$(LD) $^ -o bin/pc $(LIBS) -DNO_LOGS
list_debug: $(addprefix $(LD), $(DEPS))
	$(CC) -g $(FLAGS) -I./$(LD) $^ -o bin/debug_pc $(LIBS)


#successive cancellation decoder
sc_release: $(addprefix $(SCD), $(DEPS))
	$(CC) $(OPT) $(FLAGS) -I./$(SCD) $^ -o bin/pc $(LIBS) -DNO_LOGS
sc_debug: $(addprefix $(SCD), $(DEPS))
	$(CC) -g $(FLAGS) -I./$(SCD) $^ -o bin/debug_pc $(LIBS)


# tests
tests: $(TEST_MAIN) $(TEST_DEPS) $(DEPS)
	$(CC) -g $^ $(FLAGS) $(LIBS) -o bin/test_pc
