#ifndef _SEQUENTIAL_DECODER_H_
#define _SEQUENTIAL_DECODER_H_

#include <algorithm>
#include <vector>
#include <set>
#include "./common.h"
#include "./stack.h"
#include "./code.h"

#define SET
// #define BINARY_HEAP
// #define MINMAX_HEAP

typedef std::pair<double, int32_t> PAIR;

bool compare(const PAIR & a, const PAIR & b);

class SequentialDecoder {
  private:
    const int32_t n, k, m, L, Theta;
    bool *frozen;
    bool ****arrayPointer_C;
    int32_t *fi;
    int32_t *q;
    int32_t **arrayReferenceCount;
    int32_t **pathIndexToArrayIndex;
    double stddev;
    double *O;  // Omega: O[i] = product (1 - P_j), j > i, j in F
    double **D;
    double ***arrayPointer_S;
    Stack<int32_t> inactivePathIndeces;
#ifdef SET
    std::set<PAIR, bool(*)(const PAIR &, const PAIR &)> paths;
#endif
#ifdef BINARY_HEAP
    BinaryHeap<PAIR, &compare> paths;
#endif
#ifdef MINMAX_HEAP
    MinMaxHeap<PAIR, &compare> paths;
#endif
    std::vector<Stack<int32_t> > inactiveArrayIndeces;

  public:
    SequentialDecoder(int32_t n, int32_t k, int32_t L, int32_t theta,
                      double stddev, int32_t *indeces, double *O);
    ~SequentialDecoder();

    /// @param received vector to decode
    /// @return decoded codeword
    void decode(double *y, bool *c);

  private:
    /// @return index l of initial path
    int32_t assignInitialPath();

    /// @param index l of path to clone
    /// @return index l' to copy
    int32_t clonePath(int32_t l);

    /// @param index l of path to kill
    void killPath(int32_t l);

    // returns private copy of ArrayPointer_P if ReferenceCount > 1
    /// @param layer lambda
    /// @param path index l
    /// @return pointer to corresponding probability pair array
    double *getArrayPointer_S(int32_t lambda, int32_t l);

    // returns private copy of ArrayPointer_C if ReferenceCount > 1
    /// @param layer lambda
    /// @param path index l
    /// @return pointer to corresponding bit pair array
    bool **getArrayPointer_C(int32_t lambda, int32_t l);

    // returns private copy of D if ReferenceCount > 1
    /// @param layer lambda
    /// @param path index l
    /// @return pointer to corresponding D
    double *getPointer_D(int32_t lambda, int32_t l);

    // no private copy
    /// @param layer lambda
    /// @param path index l
    /// @return pointer to corresponding bit pair array
    const double *readArrayPointer_S(int32_t lambda, int32_t l);

    // no private copy
    /// @param layer lambda
    /// @param path index l
    /// @return pointer to corresponding bit pair array
    const bool **readArrayPointer_C(int32_t lambda, int32_t l);

    // no private copy
    /// @param layer lambda
    /// @param path index l
    /// @return pointer to corresponding D
    const double *readPointer_D(int32_t lambda, int32_t l);

    /// @param path l
    /// @param layer lambda
    /// @param phase fi
    void recursivelyCalcS(int32_t l, int32_t lambda, int32_t fi);

    /// @param path l
    /// @param layer lambda
    /// @param phase fi
    void recursivelyUpdateC(int32_t l, int32_t lambda, int32_t fi);

    void fillOmega(const int32_t *indeces, const double *z, const bool *frozen);

    inline double W(const double &y, const double &x) const {
        return 1 / (1 + exp((-1)*x*2 * y / (stddev*stddev)));
    }
    inline double initS(const double &y) const {
        return -2 * y / (stddev*stddev);
    }
    inline double Q(const double & a, const double & b) {
        return sign(a)*sign(b)*std::min(std::abs(a), std::abs(b));
    }
    template <class T>
    inline int32_t sign(T val) {
        return (T(0) < val) - (val < T(0));
    }
    void clean_data_structures();
};

#endif /* _SEQUENTIAL_DECODER_H_ */
