#ifndef _CODE_H_
#define _CODE_H_

#include <iostream>
#include <string>
#include <random>
#include "./common.h"

/// input methods
void gen_random_input(bool *u, int32_t K);
void input_seq(std::string &input, bool *u, int32_t K);

/// Prints sequence.
template<typename T>
void println_seq(T *seq, int32_t n, bool space = false) {
  T *p = seq;
  if (!space) {
    for (int32_t i = 0; i < n; ++i) {
      std::cout << *p << " ";
      ++p;
    }
    std::cout << std::endl;
  } else {
    for (int32_t i = 0; i < n; ++i) {
      std::cout << *p;
      ++p;
    }
    std::cout << std::endl;
  }
}

#endif /* _CODE_H_ */
