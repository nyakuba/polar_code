#ifndef _ENCODING_H_
#define _ENCODING_H_

#include <algorithm>
#include <utility>
#include "./common.h"
#include "./code.h"

/// Shuffles seq.
void reverse_shuffle(int32_t *seq, int32_t n, int32_t *tmp, int32_t *work);
/// computes Bhattacharyya paramaters for BEC channels
void initZ_BEC(double *z, int32_t n, double erasure_prob);
/// encoding
void encode(bool *u, bool *v, int32_t n, int32_t *shuffle);

/// sorts associated arrays
template<typename T1, typename T2>
void sortZ(T1 *z, T2 *indeces, int32_t n, int32_t lo, int32_t hi) {
    if (hi - lo < 2) return;
    int32_t i = lo, j = lo, k = hi;
    while (j < k) {
        if (z[j] == z[i]) {
            ++j;
        } else if (z[j] < z[i]) {
            std::swap(z[j], z[i]);
            std::swap(indeces[j], indeces[i]);
            ++i; ++j;
        } else {
            --k;
            std::swap(z[j], z[k]);
            std::swap(indeces[j], indeces[k]);
        }
    }
    sortZ(z, indeces, n, lo, i);
    sortZ(z, indeces, n, j, hi);
}

#endif /* _ENCODING_H_ */
