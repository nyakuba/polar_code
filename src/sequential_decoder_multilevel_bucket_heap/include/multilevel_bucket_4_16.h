#ifndef _MULTILEVEL_BUCKET_H_1
#define _MULTILEVEL_BUCKET_H_1

#include <iostream>
#include <cstdint>
#include <cassert>
#include <cstring>
#include <ctime>
#include "stack.h"

#define CMP_COUNT
#define TIME_COUNT

// We can use min bucket heap because we can estimate upper bound of values.
// Works only with integers.
class BucketHeap {
      public:
        class iterator;
        typedef std::pair<int64_t, int32_t> PAIR;
        typedef int64_t key_type;
        typedef int32_t index_t;
        typedef int32_t size_t;
      private:
        static const int32_t _k = 16; // level count
        static const int32_t _base_bits = 4;
        static const key_type _base = (key_type)1 << _base_bits; // determines buckets count. must = 2^_base_bits
        static const key_type _first_digit_mask = (int64_t)7 << 60;
        static const key_type _second_digit_mask = (int64_t)15 << 56;
        static const int32_t _digits = 16; // number of digits in _base representation of key_type

        const size_t _capacity; // maximum heap size

        key_type _last_extracted; // last extracted minimum from heap
        PAIR *_keys;
        index_t *_left;
        index_t **_buckets;
        Stack<index_t> _free_indeces;
        
        uint64_t _empty_levels; // i-th bit of this number determines i level state
        uint64_t *_empty_buckets; 
        
#ifdef CMP_COUNT
        uint64_t cmp_counter;
#endif
#ifdef TIME_COUNT
        clock_t total_time;
#endif
        
        size_t _size;
        size_t *_level_size;

      public:
        BucketHeap(size_t _capacity):
            _capacity(_capacity),
            _last_extracted(0), 
            _free_indeces(_capacity),
            _empty_levels(0),
#ifdef CMP_COUNT
            cmp_counter(0),
#endif
#ifdef TIME_COUNT
            total_time(0),
#endif
            _size(0)
        {
            _keys = new PAIR[_capacity];
            _left = new index_t[_capacity];
            _buckets = new index_t*[_k+1];
            for (int32_t i = 1; i <= _k; ++i) {
                _buckets[i] = new index_t[_base];
                for (int32_t j = 0; j < _base; ++j) {
                    _buckets[i][j] = -1;
                }
            }
            _buckets[0] = new index_t[1];
            _buckets[0][0] = -1;
            _level_size = new size_t[_k+1];
            memset(_level_size, 0, sizeof(size_t)*(_k+1));
            for (int32_t i = 0; i < _capacity; ++i) {
                _free_indeces.push(i);
            }
            _empty_buckets = new uint64_t[_k+1];
            memset(_empty_buckets, 0, sizeof(uint64_t)*(_k+1));
        }
        
        ~BucketHeap() {
            delete [] _keys;
            delete [] _left;
            delete [] _level_size;
            for (int32_t i = 0; i <= _k; ++i) {
                delete [] _buckets[i];
            }
            delete [] _buckets;
            delete [] _empty_buckets;
        }
        
        size_t capacity() const {
            return _capacity;
        }

        size_t size() const {
            return _size;
        }
        
        bool empty() const {
            return _size == 0;
        }

        void push(PAIR key) {
            assert(_size <= _capacity);
#ifdef TIME_COUNT
            clock_t start = clock();
#endif
            index_t index = _free_indeces.top();
            _free_indeces.pop();
            _keys[index] = key;
            index_t i, j;
            if (key.first < _last_extracted) {
                i = 0; j = 0;
            } else {
                calculate_first_diff_digit_position(key.first, i, j);
                ++i; // zero bucket
            }
#ifdef CMP_COUNT
            ++cmp_counter;
#endif
            ++_level_size[i];
            set_bit(_empty_levels, i);
            set_bit(_empty_buckets[i], j);
            insert_key_into_bucket(index, _buckets[i][j]);
            ++_size;
#ifdef TIME_COUNT
            clock_t end = clock();
            total_time += end - start;
#endif
        }
        
        PAIR extract_min() {
            assert(_size > 0);
#ifdef TIME_COUNT
            clock_t start = clock();
#endif
            // определяем первый пустой уровень и первый пустой карман.
            index_t first_nonempty_level = first_nonempty(_empty_levels, _k);
            index_t first_nonempty_bucket = first_nonempty(_empty_buckets[first_nonempty_level], _base);
            // вытаскиваем минимум из кармана.
            index_t min = extract_min_from_bucket(_buckets[first_nonempty_level][first_nonempty_bucket]);
            --_level_size[first_nonempty_level];
            if (_buckets[first_nonempty_level][first_nonempty_bucket] == -1) {
                clear_bit(_empty_buckets[first_nonempty_level], first_nonempty_bucket);
            }
            if (_level_size[first_nonempty_level] == 0) {
                clear_bit(_empty_levels, first_nonempty_level);
            }
            // определяем позицию первой отличающейся цифры минимума и последнего извлеченного элемента.
            index_t i, j;
            calculate_first_diff_digit_position(_keys[min].first, i, j);
            ++i; // zero bucket
            _last_extracted = _keys[min].first;
            _free_indeces.push(min);
            // update buckets.
            if (first_nonempty_level == 0) {
                for (index_t level = i; level > 0; --level) {
                    if (_level_size[level] == 0) {
                        continue;
                    }
                    for (index_t bucket = 0; bucket < _base; ++bucket) {
                        update_bucket(level, bucket);
                    }
                }
                update_bucket(0, 0);
            } else {
                update_bucket(i, j);
            }
            --_size;
#ifdef TIME_COUNT
            clock_t end = clock();
            total_time += end - start;

#endif
            return _keys[min];
        }
        
        void clear() {
            _size = 0;
            _last_extracted = 0;
            memset(_level_size, 0, sizeof(size_t)*(_k+1));
            _empty_levels = 0;
            memset(_empty_buckets, 0, sizeof(uint64_t)*(_k+1));
            for (int32_t i = 1; i <= _k; ++i) {
                for (int32_t j = 0; j < _base; ++j) {
                    _buckets[i][j] = -1;
                }
            }
            _buckets[0][0] = -1;
            _free_indeces.clear();
            for (index_t i = 0; i < _capacity; ++i) {
                _free_indeces.push(i);
            }
#ifdef CMP_COUNT
            cmp_counter = 0;
#endif
#ifdef TIME_COUNT
            total_time = 0;
#endif
        }

      private:
        void update_bucket(index_t i, index_t j) {
            if (_buckets[i][j] < 0) {
                return;
            }
            index_t tmp_bucket = -1;
            index_t _i, _j;
            index_t head = _buckets[i][j];
            while (head != -1) {
                index_t left_head = _left[head];
                index_t left_left_head = _left[left_head];
                calculate_first_diff_digit_position(_keys[left_head].first, _i, _j);
                ++_i; // zero bucket
                if (_i != i) {
                    --_level_size[i];
                    ++_level_size[_i];
                    set_bit(_empty_levels, _i);
                    set_bit(_empty_buckets[_i], _j);
                    insert_key_into_bucket(left_head, _buckets[_i][_j]);
                } else {
                    if (j == _j) {
                        insert_key_into_bucket(left_head, tmp_bucket);
                    } else {
                        // TODO is more efiicient to check if level_size == 0?
                        set_bit(_empty_buckets[_i], _j);
                        insert_key_into_bucket(left_head, _buckets[_i][_j]);
                    }
                }

                if (head == left_head) {
                    head = -1;
                    _buckets[i][j] = tmp_bucket;
                    if (_buckets[i][j] == -1) {
                        clear_bit(_empty_buckets[i], j);
                    }
                    if (_level_size[i] == 0) {
                        clear_bit(_empty_levels, i);
                    }
                } else {
                    _left[head] = left_left_head;
                }
            }
        }
        
        // inserts key into specified bucket.
        // bucket is a reference to number, that points to head's index in _keys and _left array.
        void insert_key_into_bucket(index_t index, index_t & bucket) {
            if (bucket == -1) {
                bucket = index;
                _left[index] = index;
            } else {
                _left[index] = _left[bucket];
                _left[bucket] = index;
                if (_keys[index].first < _keys[bucket].first) {
                    bucket = index;
                }
#ifdef CMP_COUNT
                ++cmp_counter;
#endif
            }
        } 

        // extracts minimum from bucket, decrement size value and clear flags if needed
        // than method finds another minimum and sets bucket's head to that minimum
        // method does not free element's memory
        index_t extract_min_from_bucket(index_t & bucket) {
            assert(bucket >= 0);
            index_t index = _left[bucket];
            if (index == bucket) {
                bucket = -1;
            } else {
                std::swap(_keys[bucket], _keys[index]);
                _left[bucket] = _left[index];
  
                index_t min = bucket;
                index_t it = _left[bucket];
                while (it != bucket) {
                    if (_keys[it].first < _keys[min].first) {
                        min = it;
                    }
#ifdef CMP_COUNT
                    ++cmp_counter;
#endif
                    it = _left[it];
                }
                bucket = min;
            }            
            return index;
        }
        
        
        // Helper functions
        // zero based numeration
        inline void set_bit(uint64_t & value, const index_t & position) {
            value |=  ((uint64_t)1 << (uint64_t)position);
        }

        inline void clear_bit(uint64_t & value, const index_t & position) {
            value &= ~((uint64_t)1 << (uint64_t)position);
        }

        // method finds first nonzero bit in first 17 bits of uint32_t
        // hbp - highest possible nonzero bit
        index_t first_nonempty(uint64_t flags, int32_t hbp) {
            assert(flags != 0);
            uint64_t level_bit = (~(flags - 1)) & flags;
            index_t hi_bit_pos = hbp;
            index_t lo_bit_pos = 0;
            index_t mid_bit_pos = hi_bit_pos/2;
            uint64_t mid = (uint64_t)1 << mid_bit_pos;
            while (hi_bit_pos > lo_bit_pos+1) {
                if (level_bit == mid) {
                    return mid_bit_pos;
                } else if (level_bit > mid) {
                    lo_bit_pos = mid_bit_pos;
                } else {
                    hi_bit_pos = mid_bit_pos;
                }
                mid_bit_pos = (hi_bit_pos + lo_bit_pos)/2;
                mid = (uint64_t)1 << mid_bit_pos;
            }
            if (level_bit > mid) {
                return hi_bit_pos;
            } else if (level_bit < mid) {
                return lo_bit_pos;
            } else {
                return mid_bit_pos;
            }
        }

        // calculate position of most significant digit of _last_extracted and key, that differs.
        // (base 2^m representation)
        void calculate_first_diff_digit_position(key_type key, index_t& i, index_t& j) {
            if (key == _last_extracted) {
                i = 0; j = key & (_base - 1);
                return;
            }
            if ((_first_digit_mask & key) != (_first_digit_mask & _last_extracted)) {
                i = _digits - 1;
                j = (_first_digit_mask & key) >> ((_digits - 1)*_base_bits);
                return;
            }
            index_t digit = _digits - 1;
            key_type mask = _second_digit_mask;
            while (digit > 0) {
                if ((key & mask) != (_last_extracted & mask)) {
                    i = digit - 1;
                    j = (key & mask) >> ((digit - 1)*_base_bits);
                    break;
                }
                --digit;
                mask >>= _base_bits;
            }
        }

        // iterator class and methods, that uses them: begin, end, erase        
      public:
        class iterator {
                friend class BucketHeap;
                typedef typename BucketHeap::PAIR PAIR;
                typedef typename BucketHeap::key_type key_type;
                typedef typename BucketHeap::size_t size_t;
                typedef typename BucketHeap::index_t index_t;
              private:
                BucketHeap *heap;
                index_t current_level;
                index_t current_bucket;
                index_t current_index;
                iterator(BucketHeap *heap, index_t level, index_t bucket, index_t index):
                    heap(heap), current_level(level), current_bucket(bucket), current_index(index) {}

              public:
                iterator operator=(const iterator & it) {
                    heap = it.heap;
                    current_level = it.current_level;
                    current_bucket = it.current_bucket;
                    current_index = it.current_index;
                    return *this;
                }
                
                bool operator==(const iterator & it) const {
                    return it.current_index == current_index;
                }
                
                bool operator!=(const iterator & it) const {
                    return !(*this == it);
                }

                const PAIR & operator*() {
                    return heap->_keys[current_index];
                }
                
                iterator operator++() {
                    if (current_index >= 0) {
                        current_index = heap->_left[current_index];
                    }
                    if (current_index == heap->_buckets[current_level][current_bucket]) {
                        ++current_bucket;
                        if (current_level == 0) {
                            current_bucket = 0;
                            ++current_level;
                        }
                        while (current_bucket < heap->_base && heap->_buckets[current_level][current_bucket] < 0) {
                            ++current_bucket;
                        }
                        if (current_bucket >= heap->_base) {
                            ++current_level;
                            while (current_level <= heap->_k && heap->_level_size[current_level] == 0) {
                                ++current_level;
                            }
                            if (current_level >= heap->_k+1) {
                                current_bucket = 0;
                                current_index = -1;
                            } else {
                                current_bucket = heap->first_nonempty(heap->_empty_buckets[current_level], _base);
                                current_index = heap->_buckets[current_level][current_bucket];
                            }
                        } else {
                            current_index = heap->_buckets[current_level][current_bucket];
                        }
                    } 
                    return *this;
                }
                
                const PAIR * operator->() {
                    return heap->_keys + current_index;
                }
        };

        iterator begin() {
#ifdef TIME_COUNT
            clock_t start = clock();
#endif
            if (_empty_levels == 0) {
                return iterator(this, _k+1, 0, -1);
            }
            index_t level = first_nonempty(_empty_levels, _k);
            index_t bucket = first_nonempty(_empty_buckets[level], _base);
#ifdef TIME_COUNT
            clock_t end = clock();
            total_time += end - start;
#endif
            return iterator(this, level, bucket, _buckets[level][bucket]); 
        }

        iterator end() {
            return iterator(this, _k+1, 0, -1);
        }

        void erase(iterator & it) {
#ifdef TIME_COUNT
            clock_t start = clock();
#endif
            index_t level = it.current_level;
            index_t bucket = it.current_bucket;
            index_t key = it.current_index;

            if (key == _buckets[level][bucket]) {
                index_t index = extract_min_from_bucket(_buckets[level][bucket]);
                _free_indeces.push(index);
                if (_buckets[level][bucket] == -1) {
                    it.current_index = -1;
                }
            } else {
                index_t left_key = _left[key];
                std::swap(_keys[key], _keys[left_key]);
                _left[key] = _left[left_key];
                _free_indeces.push(left_key);
                if (left_key == _buckets[level][bucket]) {
                    _buckets[level][bucket] = key;
                } 
            }
           
            --_level_size[level]; 
            if (_buckets[level][bucket] == -1) {
                clear_bit(_empty_buckets[level], bucket);
            }
            if (_level_size[level] == 0) {
                clear_bit(_empty_levels, level);
            }
            --_size;
            ++it;
#ifdef TIME_COUNT
            clock_t end = clock();
            total_time += end - start;
#endif
        }

        // CMP_COUNT        
#ifdef CMP_COUNT
        uint64_t get_cmp_ops_count() const {
            return cmp_counter;
        }
#else
        uint64_t get_cmp_ops_count() const {
            return 0;
        }
#endif
        // TIME_COUNT
#ifdef TIME_COUNT
        clock_t get_total_time() const {
            return total_time;
        }
#else 
        clock_t get_total_time() const {
            return 0;
        }
#endif
}; 

#endif /* _MULTILEVEL_BUCKET_H_ */
