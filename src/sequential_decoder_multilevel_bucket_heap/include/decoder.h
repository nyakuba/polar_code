#ifndef _SEQUENTIAL_DECODER_H_
#define _SEQUENTIAL_DECODER_H_

#include <algorithm>
#include <vector>
#include <set>
#include <utility>
#include "./common.h"
#include "./stack.h"
#include "./code.h"

#include "./multilevel_bucket_3_21.h"

#define OPS_COUNT

// identifies an entry within C array
struct Triple {
    uint32_t lambda;
    uint32_t beta;
    uint32_t phi;
};

typedef std::set<Triple> tExpression;
typedef typename std::pair<int64_t, int32_t> PAIR;
typedef bool* tPairOfBool;

class SequentialDecoder {
  private:
    const int32_t n, k, m, L, Theta;
    bool ****arrayPointer_C;
    int32_t *fi;
    int32_t *q;
    int32_t *p_inf_constraint_size;
    int32_t *p_decision_points;
    int32_t *p_layer_phases;
    int32_t **arrayReferenceCount;
    int32_t **pathIndexToArrayIndex;
    double stddev;
    double max_gap;
    // Omega: O[i] = product (1 - P_j), j > i, j in F
    double *O;
    double **D;
    double ***arrayPointer_S;
    Stack<int32_t> inactivePathIndeces;
    // code specification
    Triple **pp_inf_constraints;
    std::vector<Stack<int32_t> > inactiveArrayIndeces;
    BucketHeap paths;
    // operation counters
    static int64_t cmp_ops_count;
    static int64_t add_ops_count;
    static int64_t iteration;
    static int32_t run_count;
    static int64_t erase_counter;

  public:
    static int64_t iters_with_erase;

    SequentialDecoder(int32_t n, int32_t k,
                      int32_t L, int32_t theta,
                      double max_gap,
                      double stddev, double *z,
                      std::set<int32_t> *p_inf_constraints,
                      int32_t *p_decision_points);
    ~SequentialDecoder();

    /// @param received vector to decode
    /// @return decoded codeword
    void decode(double *y, bool *c);

/// cleans all working data structures
    void clean_data_structures();

    static inline bool cmp(const PAIR & a, const PAIR & b) {
        if (a.first == b.first) {
#ifdef OPS_COUNT
        // cmp_ops_count += 1;
#endif
            return a.second > b.second;
        } else {
#ifdef OPS_COUNT
        // cmp_ops_count += 2;
#endif
            return a.first > b.first;
        }
    }

    int64_t get_cmp_ops_count() const {
        return cmp_ops_count;
    }

    int64_t get_decoder_iterations() const {
        return iteration;
    }

    int64_t get_add_ops_count() const {
        return add_ops_count;
    }

    clock_t get_heap_total_time() const {
        return 0;
    }

  private:
    /// @return index l of initial path
    int32_t assignInitialPath();

    /// @param index l of path to clone
    /// @return index l' to copy
    int32_t clonePath(int32_t l);

    /// @param index l of path to kill
    void killPath(int32_t l);

    // returns private copy of ArrayPointer_P if ReferenceCount > 1
    /// @param layer lambda
    /// @param path index l
    /// @return pointer to corresponding probability pair array
    double *getArrayPointer_S(int32_t lambda, int32_t l);

    // returns private copy of ArrayPointer_C if ReferenceCount > 1
    /// @param layer lambda
    /// @param path index l
    /// @return pointer to corresponding bit pair array
    bool **getArrayPointer_C(int32_t lambda, int32_t l);

    // returns private copy of D if ReferenceCount > 1
    /// @param layer lambda
    /// @param path index l
    /// @return pointer to corresponding D
    double *getPointer_D(int32_t lambda, int32_t l);

    // no private copy
    /// @param layer lambda
    /// @param path index l
    /// @return pointer to corresponding bit pair array
    const double *readArrayPointer_S(int32_t lambda, int32_t l);

    // no private copy
    /// @param layer lambda
    /// @param path index l
    /// @return pointer to corresponding bit pair array
    const bool **readArrayPointer_C(int32_t lambda, int32_t l);

    // no private copy
    /// @param layer lambda
    /// @param path index l
    /// @return pointer to corresponding D
    const double *readPointer_D(int32_t lambda, int32_t l);

    /// @param path l
    /// @param layer lambda
    /// @param phase fi
    void recursivelyCalcS(int32_t l,
                          int32_t lambda,
                          int32_t fi);

    /// @param path l
    /// @param layer lambda
    /// @param phase fi
    void recursivelyUpdateC(int32_t l,
                            int32_t lambda,
                            int32_t fi);

    /// Erases paths with fi < min_fi
    void erasePaths(int32_t min_fi);

    /// fills Omega array, using information of frozen symbols positions
    void fillOmega(const double *z, const int32_t *p_decision_points);

    /// ???
    tExpression RecoverInfSymbol(uint32_t lambda0,
                                 uint32_t lambda,
                                 uint32_t beta,
                                 uint32_t phi);
    /// ???
    void init_pp_inf_constraints(std::set<int32_t> *p_inf_constraints);

    // utilities
    template <class T>
    inline int32_t sign(T val) {
        return (T(0) < val) - (val < T(0));
    }

    static inline bool bxor(const bool & a, const bool & b) {
        return a != b;
    }

    inline double W(const double &y, const double &x) const {
        return 1 / (1 + exp((-1)*x*2 * y / (stddev*stddev)));
    }

    inline double initS(const double &y) const {
        return -2 * y / (stddev*stddev);
    }

    inline double Q(const double & a, const double & b) {
#ifdef OPS_COUNT
        cmp_ops_count += 3;
#endif
        return sign(a)*sign(b) *
            std::min(std::abs(a), std::abs(b));
    }
};

#endif /* _SEQUENTIAL_DECODER_H_ */
