#ifndef _CHANNEL_H_
#define _CHANNEL_H_

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <random>
#include "./common.h"
#include "./code.h"

void sendAWGN(bool *v, double *y, int32_t n,
              std::default_random_engine *gen,
              std::normal_distribution<double> *dist);
void sendAWGN(bool *v, double *y, int32_t n,
              gsl_rng * r, double stddev);

#endif /* _CHANNEL_H_ */
