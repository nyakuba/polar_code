/* Copyright message here */
#include <gsl/gsl_rng.h>
#include <ctime>
#include <iostream>
#include <fstream>
#include <set>
#include <string>
#include "include/GaussianApproximation.h"
#include "include/code.h"
#include "include/common.h"
#include "include/encoding.h"
#include "include/channel.h"
#include "include/decoder.h"

int main(int argc, char *argv[]) {
    std::ios_base::sync_with_stdio(false);
    // initialization
    int32_t test_count = 0;  // codewords to decode
    int32_t iter_count = 0;  // decode iters on each codeword
    int32_t not_decoded_max_count = 0;
    int32_t N = 0;
    int32_t K = 0;
    int32_t list_size = 1;
    int32_t Theta = 1;  // queue capacity
    double snr = 0;
    // in decoder we can ignore paths with too low metric
    // epsilon-method. should be > 0
    double max_gap = 0;

    std::cout << "Enter count of codewords to decode : ";
    std::cin >> test_count;
    std::cout << test_count << std::endl;
    std::cout << "Enter count of iterations on each codeword : ";
    std::cin >> iter_count;
    std::cout << iter_count << std::endl;
    std::cout << "Enter max decoding errors limit : ";
    std::cin >> not_decoded_max_count;
    std::cout << not_decoded_max_count << std::endl;
    std::cout << "Enter: N K L queue_capacity max_gap channel_SNR" << std::endl;
    std::cin >> N >> K >> list_size >> Theta >> max_gap >> snr;
    std::cout << N << " " << K << " " << " " << list_size
              << " " << Theta << " " << max_gap << " " << snr << std::endl;

    double stddev = sqrt(1/(2*pow(10, (snr/10)) * (static_cast<double>(K)/N)));
    std::cout << "Standard deviation: " << stddev << std::endl;

    int32_t fsc = N - K;  // frozen symbols count
    // subchannel error probability
    double *p_subchannel_err_prob = new double[N];
    double *p_noisy_codeword = new double[N];  // codeword + noise
    int32_t *p_shuffle = new int32_t[N];
    int32_t *p_decision_points = new int32_t[N];
    bool *p_information_symbols = new bool[K];    // informational symbols
    bool *p_codeword = new bool[N];    // codeword
    bool *p_decoded_codeword = new bool[N];    // codeword after decoding
    bool *p_decoded_info = new bool[N];  // decoded informational symbols
    std::set<int32_t> *code_specification = new std::set<int32_t>[fsc];

    // utility vars
    clock_t start;
    clock_t end;
    clock_t total_time = 0;
    clock_t heap_t = 0;
    int64_t not_decoded = 0;
    int64_t not_decoded_iter = 0;
    int64_t cmp_count = 0;
    int64_t add_count = 0;
    int64_t total_cmp_ops_count = 0;
    int64_t total_add_ops_count = 0;
    int64_t iter_counter = 0;
    int64_t total_iter_counter = 0;
    int64_t decoder_iter_counter = 0;
    int64_t total_decoder_iters = 0;
    bool *p_tmp_bool_1 = new bool[N];
    int32_t *p_tmp_int32_1 = new int32_t[N];
    int32_t *p_tmp_int32_2 = new int32_t[N];
    std::string str_u;
    const gsl_rng_type * T;
    gsl_rng * rng;
    gsl_rng_env_setup();
    T = gsl_rng_mt19937;
    rng = gsl_rng_alloc(T);

    memset(p_shuffle, 0, sizeof(int32_t)*N);
    memset(p_subchannel_err_prob, 0, sizeof(double)*N);
    memset(p_noisy_codeword, 0, sizeof(double)*N);
    memset(p_information_symbols, 0, sizeof(bool)*K);
    memset(p_codeword, 0, sizeof(bool)*N);
    memset(p_decoded_codeword, 0, sizeof(bool)*N);
    memset(p_decoded_info, 0, sizeof(bool)*N);
    memset(p_decision_points, ~0, sizeof(int32_t)*N);
    memset(p_tmp_int32_2, 0, sizeof(int32_t)*N);
    memset(p_tmp_int32_1, 0, sizeof(int32_t)*N);
    memset(p_tmp_bool_1, 0, sizeof(bool)*N);
    for (int32_t i = 0; i < N; ++i) {
        p_shuffle[i] = i;
    }


    // 1. ************************************************** //
#ifndef NO_LOGS
    std::cout << "Calculating subchannel probabilities."
              << std::endl;
#endif

    start = clock();
    GetSubchannelQuality(log2(N), stddev, p_subchannel_err_prob);
    end = clock();
    total_time += end - start;

#ifndef NO_LOGS
    std::cout << "Time consumed: " << end - start << std::endl;
    std::cout << "Total time: " << total_time << std::endl;
    std::cout << "Forming reverse shuffle permutation array." << std::endl;
#endif

    start = clock();
    reverse_shuffle(p_shuffle, N, p_tmp_int32_2, p_tmp_int32_1);
    end = clock();
    total_time += end - start;

#ifndef NO_LOGS
    std::cout << "Time consumed: " << end - start << std::endl;
    std::cout << "Total time: " << total_time << std::endl;
    std::cout << "Code initialization completed." << std::endl;
    std::cout << "Time consumed: " << total_time << std::endl;
#endif

    // 2. ************************************************** //
#ifndef NO_LOGS
    std::cout << "Word to encode: ";
#endif
    std::cin >> str_u;
#ifndef NO_LOGS
    std::cout << str_u << std::endl;
    std::cout << "Enter code specification." << std::endl;
#endif
    // read code specification
    for (int32_t i = 0; i < fsc; ++i) {
        int32_t num_vars;
        std::cin >> num_vars;
        int32_t tmp;
        int32_t max = 0;
        for (int32_t j = 0; j < num_vars; ++j) {
            std::cin >> tmp;
            code_specification[i].insert(tmp);
            if (tmp > max) {
                max = tmp;
            }
        }
        p_decision_points[max] = i;
    }

    // stddev - channel standard deviation.
    SequentialDecoder decoder(N, K, list_size,
                              Theta, max_gap,
                              stddev, p_subchannel_err_prob,
                              code_specification,
                              p_decision_points);

    // for test_count loop
    for (int test_counter = 0; test_counter < test_count; ++test_counter) {
        if (str_u.compare("random") == 0) {
            gen_random_input(p_information_symbols, K, rng);
        } else if (str_u.compare("zero") == 0) {
            memset(p_information_symbols, 0, sizeof(bool)*K);
        } else {
            input_seq(&str_u, p_information_symbols, K);
        }
#ifndef NO_LOGS
        std::cout << "Encoding." << std::endl;
#endif

        start = clock();
        // put information into message
        // and set frozen symbols
        for (int32_t i = 0, j = 0; i < N; ++i) {
            if (p_decision_points[i] == (~0)) {
                p_tmp_bool_1[i] = p_information_symbols[j++];
            } else {
                int32_t fs = p_decision_points[i];
                if (code_specification[fs].size() != 1) {
                    for (auto && el : code_specification[fs]) {
                        if (el == i) {
                            continue;
                        } else {
                            p_tmp_bool_1[i] = p_tmp_bool_1[i] != p_tmp_bool_1[el];
                        }
                    }
                }
            }
        }
        encode(p_tmp_bool_1, p_codeword, N, p_shuffle);
        end = clock();
        total_time += end - start;

#ifndef NO_LOGS
        std::cout << "Time consumed: " << end - start << std::endl;
        std::cout << "Total time: " << total_time << std::endl;
#endif
        while ((iter_counter < iter_count) &&
               (not_decoded < not_decoded_max_count)) {
#ifndef NO_LOGS
            std::cout << "###" << std::endl;
            std::cout << "Sending encoded word over AWGN channel." << std::endl;
#endif
            start = clock();
            sendAWGN(p_codeword, p_noisy_codeword, N, rng, stddev);
            end = clock();
            total_time += end - start;
#ifndef NO_LOGS
            std::cout << "Time consumed: " << end - start << std::endl;
            std::cout << "Total time: " << total_time << std::endl;
#endif

            // 3. ************************************************** //
#ifndef NO_LOGS
            std::cout << "Decoding." << std::endl;
#endif
            start = clock();
            decoder.decode(p_noisy_codeword, p_decoded_codeword);
            encode(p_decoded_codeword, p_decoded_info, N, p_shuffle);
            end = clock();
            total_time += end - start;

#ifndef NO_LOGS
            std::cout << "Time consumed: " << end - start << std::endl;
            std::cout << "Total time: " << total_time << std::endl;
#endif

            // std::cout << "codeword \t";
            // println_seq(p_tmp_bool_1, N, true);
            // std::cout << "decoded word \t";
            // println_seq(p_decoded_info, N, true);
            int32_t error_count = 0;
            for (int i = 0; i < N; ++i) {
                if (p_decision_points[i] == (~0)) {
                    if (p_tmp_bool_1[i] != p_decoded_info[i]) {
                        ++error_count;
                    }
                }
            }

#ifndef NO_LOGS
            std::cout << "Error count : " << error_count << std::endl;
            std::cout << "Number of cmp ops: "
                      << decoder.get_cmp_ops_count() << std::endl;
            std::cout << "Number of add ops: "
                      << decoder.get_add_ops_count() << std::endl;
            std::cout << "Heap working time: "
                      << decoder.get_heap_total_time() << std::endl;
            std::cout << "Decoder iterations: "
                      << decoder.get_decoder_iterations() << std::endl;
            std::cout << std::endl;
#endif
            heap_t += decoder.get_heap_total_time();
            cmp_count += decoder.get_cmp_ops_count();
            add_count += decoder.get_add_ops_count();
            decoder_iter_counter += decoder.get_decoder_iterations();
            if (error_count > 0) {
                std::cout << "not decoded " << not_decoded_iter+1 << "/"
                          << not_decoded+1 << " iter "
                          << total_iter_counter + iter_counter
                          << std::endl;
                ++not_decoded_iter;
                ++not_decoded;
            }
            ++iter_counter;
        }  // while iter_count < max and not_decoded < max

#ifndef NO_LOGS
        std::cout << "Not decoded test cases on test "
                  << test_counter << ": " << not_decoded_iter << std::endl;
        std::cout << "Tests : " << iter_counter << std::endl;
        std::cout << "Decoder iterations : " <<
            decoder_iter_counter << std::endl;
        std::cout << "Error probability : "
                  << static_cast<double>(not_decoded_iter)/iter_counter
                  << std::endl;
        std::cout << "Total number of cmp ops: " << cmp_count << std::endl;
        std::cout << "Total number of add ops: " << add_count << std::endl;
        std::cout << "Total heap working time: " << heap_t << std::endl;
        std::cout << std::endl;
#endif
        total_iter_counter += iter_counter;
        total_cmp_ops_count += cmp_count;
        total_add_ops_count += add_count;
        total_decoder_iters += decoder_iter_counter;

        if (not_decoded >= not_decoded_max_count) {
            break;
        }

        iter_counter = 0;
        not_decoded_iter = 0;
        cmp_count = 0;
        add_count = 0;
        decoder_iter_counter = 0;
        memset(p_tmp_bool_1, 0, sizeof(bool)*N);
    }  // test_count
    std::cout << "iters with erase " << decoder.iters_with_erase << std::endl;
    std::cout << "Total number of tests: "
              << total_iter_counter << std::endl;
    std::cout << "Not decoded test cases count : "
              << not_decoded << std::endl;
    std::cout << "Total time consumed: " << total_time << std::endl;
    std::cout << "Total cmp operations: " << total_cmp_ops_count << std::endl;
    std::cout << "Total add operations: " << total_add_ops_count << std::endl;
    std::cout << "Average decoder iterations: "
              << static_cast<double>(total_decoder_iters) / total_iter_counter
              << std::endl;
    std::cout << "Average cmp ops count per test: "
              << static_cast<double>(total_cmp_ops_count) / total_iter_counter
              << std::endl;
    std::cout << "Average add ops count per test: "
              << static_cast<double>(total_add_ops_count) / total_iter_counter
              << std::endl;
    std::cout << "Total error probability: "
              << static_cast<double>(not_decoded) / total_iter_counter
              << std::endl;

    // free allocated memory
    gsl_rng_free(rng);
    delete [] p_subchannel_err_prob;
    delete [] p_noisy_codeword;
    delete [] p_shuffle;
    delete [] p_information_symbols;
    delete [] p_codeword;
    delete [] p_decoded_codeword;
    delete [] p_decoded_info;
    delete [] code_specification;
    delete [] p_decision_points;
    delete [] p_tmp_bool_1;
    delete [] p_tmp_int32_1;
    delete [] p_tmp_int32_2;

    return 0;
}
