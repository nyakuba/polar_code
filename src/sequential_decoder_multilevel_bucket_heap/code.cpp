/* Copyright message here */
#include <string>
#include "include/code.h"

void input_seq(std::string *input, bool *u, int32_t K) {
    for (int i = 0; i < K; ++i) {
        switch ((*input)[i]) {
          case '0':
              u[i] = false;
              break;
          case '1':
              u[i] = true;
              break;
          default:
              std::cout << "invalid input" << std::endl;
              return;
        }
    }
}

void gen_random_input(bool *u, int32_t K) {
    std::mt19937 g;
    for (int i = 0; i < K; ++i) {
        int32_t rnd = g();
        switch (rnd & 1) {
          case 0: u[i] = true; break;
          case 1: u[i] = false; break;
        }
    }
}

void gen_random_input(bool *u, int32_t K, std::mt19937 *g) {
    for (int i = 0; i < K; ++i) {
        int32_t rnd = (*g)();
        switch (rnd & 1) {
          case 0: u[i] = true; break;
          case 1: u[i] = false; break;
        }
    }
}

void gen_random_input(bool *u, int32_t K, gsl_rng * r) {
    for (int i = 0; i < K; ++i) {
        int32_t rnd = gsl_rng_uniform_int(r, 2);
        switch (rnd) {
          case 0: u[i] = false; break;
          case 1: u[i] = true; break;
        }
    }
}
