/* Copyright message here. */
#include "include/encoding.h"

void initZ_BEC(double *z, int32_t n, double erasure_prob) {
    z[0] = erasure_prob;
    int32_t hi = 1;
    double current = 0;
    while (hi < n) {
        for (int32_t i = hi-1; i >= 0; --i) {
            current = z[i];
            z[2*i] = 2*current - current*current;
            z[2*i+1] = current*current;
        }
        hi <<= 1;
    }
}

// void initZ_BEC(double *z, int32_t n, double erasure_prob) {
//   z[0] = erasure_prob;
//   z[1] = z[0]*z[0];
//   int32_t lo = 1;
//   int32_t hi = 2;
//   while (hi < n) {
//     for (int32_t i = lo; i < hi; ++i) {
//       z[2*i] = 2*z[i] - z[i]*z[i];
//       z[2*i+1] = z[i]*z[i];
//     }
//     lo <<= 1;
//     hi <<= 1;
//   }
// }

void add_gf2(bool *res, bool *v, int32_t l) {
    for (int i = 0; i < l; ++i) {
        res[i] = res[i] != v[i];
    }
}

void mulF(bool *v, int32_t n) {
    if (n == 2) {
        v[0] = v[0] != v[1];
        return;
    }
    int32_t l = n/2;
    add_gf2(v, v+l, l);
    mulF(v, l);
    mulF(v+l, l);
}

void encode(bool *u, bool *v, int32_t n, int32_t *shuffle) {
    for (int i = 0; i < n; ++i) {
        v[i] = u[shuffle[i]];
    }
    // std::cout << "after shuffle" << std::endl;
    // println_seq(v, n, true);
    mulF(v, n);
}

void reverse_shuffle(int32_t *seq, int32_t n, int32_t *tmp, int32_t *work) {
    assert(n > 0);
    if (n <= 2) return;
    int32_t *p1 = work;
    int32_t *p2 = tmp;
    int32_t hi_bit = n/2;
    // init work with 1, 2 .. n
    for (int32_t i = 0; i < n; ++i) {
        *p1 = i;
        ++p1;
    }

    // magic with numbers
    p1 = work;
    for (int32_t i = 0; i < n; ++i) {
        if (*p1 >= hi_bit) {
            *p1 ^= hi_bit;
            *p1 <<= 1;
            ++(*p1);
        } else {
            *p1 <<= 1;
        }
        ++p1;
    }

    // copy seq to tmp in correct order
    p1 = work;
    p2 = tmp;
    for (int32_t i = 0; i < n; ++i) {
        *p2 = seq[*p1];
        ++p2; ++p1;
    }

    // fill seq.
    p1 = seq;
    p2 = tmp;
    for (int32_t i = 0; i < n; ++i) {
        *p1 = *p2;
        ++p1; ++p2;
    }

    reverse_shuffle(seq, hi_bit, tmp, work);
    reverse_shuffle(seq+hi_bit, hi_bit, tmp, work);
}
