/* Copyright message here. */
#include <gsl/gsl_sf_erf.h>
#include <gsl/gsl_math.h>
#include <algorithm>
#include "include/GaussianApproximation.h"

// see CHUNG et al.: ANALYSIS OF SUM-PRODUCT DECODING
// OF LOW-DENSITY PARITY-CHECK CODES, eq. (8)
const double Alpha = -0.4527;
const double Beta = 0.0218;
const double Gamma = 0.86;
double ChungPhi(double x) {
    return std::min(1.0, exp(Alpha*pow(x, Gamma) + Beta));
}

double ChungPhiInverse(double y) {
    if (y == 1.0) return 0;
    return pow((log(y) - Beta) / Alpha, 1 / Gamma);
}

// compute M[LLR(X+Y mod 2)] given M[LLR(X)]=M[LLR(Y)]
double XORDensity(double SrcLLRMean) {
    double OutputLLRMean = ChungPhiInverse(
        1 - pow(1 - ChungPhi(SrcLLRMean), 2));
    if (OutputLLRMean == HUGE_VAL)
        // approximate values for very large input
        OutputLLRMean = SrcLLRMean + M_LN2 / (Alpha*Gamma);
    return OutputLLRMean;
}

double SumDensity(double SrcLLRMean) {
    return 2 * SrcLLRMean;
}

/// @param log_2(code length)
/// @param AWGN standard deviation
/// @param noise standard deviation for each subchannel
void GetPolarChannelNoise(unsigned m,
                          double sigma,
                          double* pNoiseStdDev) {
    pNoiseStdDev[0] = 2 / (sigma*sigma);
    // compute LLR mean for each channel
    for (unsigned N = 2; N <= 1u << m; N <<= 1) {
        for (int i = N / 2 - 1; i >= 0; --i) {
            double  Src = pNoiseStdDev[i];
            pNoiseStdDev[2 * i + 1] = SumDensity(Src);
            pNoiseStdDev[2 * i] = XORDensity(Src);
        }
    }
    // convert it to standard deviation
    for (unsigned i = 0; i < (1u << m); ++i) {
        pNoiseStdDev[i] = sqrt(2 / pNoiseStdDev[i]);
    }
}

/// Gaussian approximation for density evolution
/// @param log_2(code length)
/// @param AWGN standard deviation
/// @param estimated error probability
void GetSubchannelQuality(unsigned m,
                          double sigma,
                          double* pChannelErrorProb) {
    double ChannelLLRMean = 2 / (sigma*sigma);
    pChannelErrorProb[0] = ChannelLLRMean;
    for (unsigned i = 0; i < m; ++i) {
        for (int j = (1u << i) - 1; j >= 0; --j) {
            double x = pChannelErrorProb[j];
            pChannelErrorProb[2 * j] = XORDensity(x);
            pChannelErrorProb[2 * j + 1] = SumDensity(x);
        }
    }
    // convert channel log-likelihoods to probabilities
    for (unsigned i = 0; i < (1u << m); i++) {
        // compute probability that an entry becomes negative
        double S = sqrt(2.0 / pChannelErrorProb[i]);
        double P = 0.5*gsl_sf_erfc(1 / (M_SQRT2*S));
        // compute the probability that neither of entries is negative
        // P = 1 - pow(1 - P, (1u << Ext) - 1.0);
        if (P > 0.31) {
            P = 0.31;
        }
        pChannelErrorProb[i] = P;
    }
}

// Gaussian approximation for density evolution
// void GetSubchannelQuality(unsigned m,  // log_2(code length)
//                           double sigma,  // AWGN standard deviation
//                           double* pErrProb  // estimated error probability
//                           ) {
//     unsigned CodeLength = 1 << m;
//     double* pTemp1 = new double[CodeLength];
//     double* pTemp2 = new double[CodeLength];
//     double ChannelLLRMean = 2 / (sigma*sigma);
//     for (unsigned i = 0; i < CodeLength; i++) {
//         pTemp1[i]=ChannelLLRMean;
//     }
//     // compute LLR mean for each channel
//     for (unsigned N = 2; N <= CodeLength; N <<= 1) {
//         for (int i = 0; i < (int)N/2; i++) {
//             double Src = pTemp1[i];
//             pTemp2[2*i+1]=SumDensity(Src);
//             pTemp2[2*i]=XORDensity(Src);
//         }
//         std::swap(pTemp1, pTemp2);
//     }
//     // convert LLR means to error probabilities
//     for (unsigned i = 0; i < CodeLength; i++) {
//         double S = sqrt(2.0/pTemp1[i]);
//         pErrProb[i]=0.5*gsl_sf_erfc(1/(M_SQRT2*S));
//     }
//     delete[]pTemp1;
//     delete[]pTemp2;
// }
