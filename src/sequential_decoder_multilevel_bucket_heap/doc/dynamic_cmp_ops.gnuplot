set terminal png
set title "Average cmp ops count per decoder iteration"
set output "dyn_cmp_ops.png"
set xlabel "SNR, db"
set ylabel "Operations count"
set grid xtics ytics mytics
set mytics 10
set style line 5 lt 1 lw 1 lc rgb "red" pi -1 pt 7 ps 1.5
plot "plot_ops_count_snr" using 1:2 title "cmp ops" w lp ls 5