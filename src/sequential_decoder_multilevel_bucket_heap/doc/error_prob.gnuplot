set terminal png
set title "Error probability on sequential decoder with zero and dynamic frozen symbols"
set output "error_prob.png"
set xlabel "SNR, db"
set ylabel "Error probability"
set grid xtics ytics mytics
set mytics 10
set logscale y
set style line 5 lt 1 lw 1 lc rgb "red" pi -1 pt 7 ps 1.5
set style line 6 lt 1 lw 1 lc rgb "blue" pi -1 pt 7 ps 1.5
set style line 7 lt 1 lw 1 lc rgb "green" pi -1 pt 7 ps 1.5
set multiplot
plot "plot_error_prob_snr" using 1:2 title "zero" w lp ls 5, \
     "plot_error_prob_snr" using 1:3 title "dynamic" w lp ls 6
unset multiplot