#/bin/bash
input=1024_512.input
echo "" > out #clean file

# echo "test 1: SNR = 1"
# (awk 'NR<3' $input && echo "2 1"    && awk 'NR>3' $input) | ./pc >> out
# echo $'\r' >> out #extra carriage return

# echo "test 1: SNR = 1.25"
# (awk 'NR<3' $input && echo "2 1.25" && awk 'NR>3' $input) | ./pc >> out
# echo $'\r' >> out #extra carriage return

# echo "test 1: SNR = 1.5"
# (awk 'NR<3' $input && echo "2 1.5"  && awk 'NR>3' $input) | ./pc >> out
# echo $'\r' >> out #extra carriage return

# echo "test 1: SNR = 1.75"
# (awk 'NR<3' $input && echo "2 1.75" && awk 'NR>3' $input) | ./pc >> out
# echo $'\r' >> out #extra carriage return

# echo "test 1: SNR = 2"
# (awk 'NR<3' $input && echo "2 2"    && awk 'NR>3' $input) | ./pc >> out
# echo $'\r' >> out #extra carriage return

echo "test 1: SNR = 2.25"
(awk 'NR<3' $input && echo "2 2.25" && awk 'NR>3' $input) | ./pc >> out
echo $'\r' >> out #extra carriage return

echo "test 1: SNR = 2.5"
(awk 'NR<3' $input && echo "2 2.5"  && awk 'NR>3' $input) | ./pc >> out
echo $'\r' >> out #extra carriage return

echo "test 1: SNR = 2.75"
(awk 'NR<3' $input && echo "2 2.75" && awk 'NR>3' $input) | ./pc >> out
echo $'\r' >> out #extra carriage return

echo "test 1: SNR = 3"
(awk 'NR<3' $input && echo "2 3"    && awk 'NR>3' $input) | ./pc >> out
echo $'\r' >> out #extra carriage return
