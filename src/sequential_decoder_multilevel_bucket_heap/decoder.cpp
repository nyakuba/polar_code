/* Copyright message here. */
#include <algorithm>
#include <set>
#include "include/decoder.h"

// #define LOG_FLOW
// #define LOG_EXTRACT
// #define LOG_INSERT

int64_t SequentialDecoder::cmp_ops_count = 0;
int64_t SequentialDecoder::add_ops_count = 0;
int64_t SequentialDecoder::iteration = 0;
int64_t SequentialDecoder::erase_counter = 0;
int64_t SequentialDecoder::iters_with_erase = 0;

void SequentialDecoder::decode(double *y, bool *c) {
#ifdef LOG_FLOW
    std::cout << "start decode" << std::endl;
#endif
    bool is_min_fi_set = false;
    int32_t min_fi = 0;
    // cleaning and init
    erase_counter = 0;
    clean_data_structures();
    int32_t l = assignInitialPath();
    fi[l] = 0;
    *(getPointer_D(0, l)) = 0;
    double *Sl = getArrayPointer_S(0, l);
    double MC = 0;
    for (int32_t beta = 0; beta < n; ++beta) {
        Sl[beta] = initS(y[beta]);
        double tmp = abs(Sl[beta]);
        if (MC < tmp) {
            MC = tmp;
        }
    }
    MC *= n;
#ifdef OPS_COUNT
        cmp_ops_count += 2*n;
#endif

    double max_metric = 0;
    paths.push(PAIR(*reinterpret_cast<int64_t*>(&MC), l));
#ifdef LOG_INSERT
    std::cout << "insert init: " << 0 << ", path " << l << std::endl;
#endif

    // main loop
    while (!paths.empty()) {
        ++iteration;
        // extract path number whith max estimate
        const PAIR p = paths.extract_min();
#ifdef LOG_EXTRACT
        std::cout << "extract: "
                  << MC - *reinterpret_cast<double*>(
                      const_cast<int64_t*>(&p.first))
                  << ", path " << l << std::endl;
#endif
        l = p.second;
        int32_t phi = fi[l];
        assert(phi <= n && phi >= 0);
        if (phi < min_fi) {
            killPath(l);
            continue;
        }
        ++q[phi];

        // stopping condition
        if (phi == n) {
            const bool **Cl = readArrayPointer_C(0, l);
            for (int32_t beta = 0; beta < n; ++beta) {
                c[beta] = Cl[beta][0];
            }
            if (erase_counter > 0) {
                ++iters_with_erase;
            }
            return;
        }

        if (p_decision_points[phi] != (~0)) {
            // FROZEN BIT
            // calculate value of frozen symbol
            const Triple* pE = pp_inf_constraints[p_decision_points[phi]];
            unsigned ICS = p_inf_constraint_size[p_decision_points[phi]];
            bool cur_value = false;
            for (unsigned i = 0; i < ICS; ++i) {
                const Triple &T = pE[i];
                const bool **pC0 = readArrayPointer_C(T.lambda, l);
                cur_value ^= pC0[T.beta][T.phi];
            }

            // extend the path with the recovered value
            bool **Clm = getArrayPointer_C(m, l);
            Clm[0][phi & 1] = cur_value;

            paths.push(p);
#ifdef LOG_INSERT
            std::cout << "insert   frozen: "
                      << iteration
                      << ": path " << l
                      << ": phi " << fi[l]
                      << ", value "
                      << MC - *reinterpret_cast<double*>(
                          const_cast<int64_t*>(&p.first))
                      << " - " << Clm[0][phi&1]
                      << std::endl;
#endif
            if ((phi & 1) == 1) {
                recursivelyUpdateC(l, m, phi);
            }
            ++fi[l];
        } else {
            // UNFROZEN BIT
            recursivelyCalcS(l, m, phi);

            // Theta must be > 1, much greater than L
            if (paths.size() >= paths.capacity()-1) {
                if (is_min_fi_set) {
                    erasePaths(min_fi);
                    is_min_fi_set = false;
                } else {
                    // while (paths.size() >= paths.capacity()-1) {
                    PAIR p = paths.extract_max();
                    killPath(p.second);
                    // }
                }
            }

            const double *Slm = readArrayPointer_S(m, l);
            const double *Dlm = readPointer_D(m, l);

            double est1 = MC - (Slm[0] + *Dlm + O[phi]);
            double est2 = MC - (*Dlm + O[phi]);
#ifdef OPS_COUNT
            add_ops_count += 5;
#endif
            max_metric = *reinterpret_cast<double*>(
                const_cast<int64_t*>(&p.first));
            // std::cout << max_metric << std::endl;
            // std::cout << est1 << std::endl;
            // std::cout << est2 << std::endl;
            if (est1 - max_metric <= max_gap &&
                est2 - max_metric <= max_gap) {
                bool **Clm = getArrayPointer_C(m, l);
                Clm[0][phi & 1] = false;

                int32_t l1 = clonePath(l);

                bool **Cl1m = getArrayPointer_C(m, l1);
                Cl1m[0][phi & 1] = true;

                paths.push(PAIR(*reinterpret_cast<int64_t*>(&est1), l));
                paths.push(PAIR(*reinterpret_cast<int64_t*>(&est2), l1));
#ifdef LOG_PATHS
                std::cout << "insert unfrozen "
                          << iteration
                          << ": path " << l
                          << ": phi " << fi[l]
                          << ", value " << Slm[0] + *Dlm + O[phi]
                          << " - " << Clm[0][phi&1]
                          << std::endl;
                std::cout << "insert unfrozen "
                          << iteration
                          << ": path " << l1
                          << ": phi " << fi[l1]
                          << ", value " << *Dlm + O[phi]
                          << " - " << Clm[0][phi&1]
                          << std::endl;
#endif
                if ((phi & 1) == 1) {
                    recursivelyUpdateC(l, m, phi);
                    recursivelyUpdateC(l1, m, phi);
                }
                ++phi;
                fi[l] = phi;
                fi[l1] = phi;
            } else if (max_metric - est1 > max_gap &&
                       max_metric - est2 > max_gap) {
                    killPath(l);
            } else if (est1 - max_metric > max_gap) {
                bool **Clm = getArrayPointer_C(m, l);
                Clm[0][phi & 1] = true;
                paths.push(PAIR(*reinterpret_cast<int64_t*>(&est2), l));
#ifdef LOG_PATHS
                std::cout << "insert unfrozen "
                          << iteration
                          << ": path " << l
                          << ": phi " << fi[l]
                          << ", value " << *Dlm + O[phi]
                          << " - " << Clm[0][phi&1]
                          << std::endl;
#endif
                if ((phi & 1) == 1) {
                    recursivelyUpdateC(l, m, phi);
                }
                ++phi;
                fi[l] = phi;
            } else if (est2 - max_metric > max_gap) {
                bool **Clm = getArrayPointer_C(m, l);
                Clm[0][phi & 1] = false;
                paths.push(PAIR(*reinterpret_cast<int64_t*>(&est1), l));
#ifdef LOG_PATHS
                std::cout << "insert unfrozen "
                          << iteration
                          << ": path " << l
                          << ": phi " << fi[l]
                          << ", value " << Slm[0] + *Dlm + O[phi]
                              << " - " << Clm[0][phi&1]
                          << std::endl;
#endif
                if ((phi & 1) == 1) {
                    recursivelyUpdateC(l, m, phi);
                }
                ++phi;
                fi[l] = phi;
            }

            if ((q[phi] >= L) && (phi > min_fi)) {
                min_fi = phi;
                is_min_fi_set = true;
            }
        }
    }
    std::cout << "invalid exit!!" << std::endl;
}

void SequentialDecoder::recursivelyCalcS(int32_t l,
                                         int32_t lambda,
                                         int32_t fi) {
#ifdef LOG_FLOW
    std::cout << "start recursivelyCalcS" << std::endl;
#endif
    if (lambda == 0) {
        return;
    }
    if (p_layer_phases[l*m + lambda] == fi) {
        return;
    }
    // assert(p_layer_phases[l*m + lambda] < fi);
    p_layer_phases[l*m + lambda] = fi;
    recursivelyCalcS(l, lambda-1, fi/2);

    int32_t size = (1 << (m-lambda));
    double *Dll = getPointer_D(lambda, l);
    double *Sll = getArrayPointer_S(lambda, l);
    const double *Dll_1 = readPointer_D(lambda-1, l);
    const double *Sll_1 = readArrayPointer_S(lambda-1, l);
    *Dll = *Dll_1;
    if ((fi & 1) == 0) {
        for (int32_t beta = 0; beta < size; ++beta) {
            Sll[beta] = Q(Sll_1[2*beta], Sll_1[2*beta+1]);
            *Dll += std::max(Sll_1[2*beta], Sll_1[2*beta+1]);
#ifdef OPS_COUNT
            cmp_ops_count += 1;
            add_ops_count += 1;
#endif
        }
    } else {
        const bool **Cll = readArrayPointer_C(lambda, l);
        for (int32_t beta = 0; beta < size; ++beta) {
            if (Cll[beta][0] == false) {
                Sll[beta] = Sll_1[2*beta+1] + Sll_1[2*beta];
#ifdef OPS_COUNT
                add_ops_count += 1;
#endif
            } else {
                Sll[beta] = Sll_1[2*beta+1] - Sll_1[2*beta];
                *Dll += Sll_1[2*beta];
#ifdef OPS_COUNT
                add_ops_count += 2;
#endif
            }
        }
    }
#ifdef LOG_FLOW
    std::cout << "end recursivelyCalcS" << std::endl;
#endif
}

void SequentialDecoder::recursivelyUpdateC(int32_t l,
                                           int32_t lambda,
                                           int32_t fi) {
#ifdef LOG_FLOW
    std::cout << "start recursivelyUpdateC" << std::endl;
#endif
    assert((fi & 1) == 1);
    int32_t psi = fi/2;
    int32_t psim2 = psi & 1;
    int32_t size = 1 << (m - lambda);
    bool **Cl_1 = getArrayPointer_C(lambda - 1, l);
    const bool **Cl = readArrayPointer_C(lambda, l);
    for (int32_t beta = 0; beta < size; ++beta) {
        Cl_1[ 2*beta ][psim2] = bxor(Cl[beta][0], Cl[beta][1]);
        Cl_1[2*beta+1][psim2] = Cl[beta][1];
    }
    if (psim2 == 1) {
        recursivelyUpdateC(l, lambda - 1, psi);
    }
#ifdef LOG_FLOW
    std::cout << "end recursivelyUpdateC" << std::endl;
#endif
}

int32_t SequentialDecoder::assignInitialPath() {
#ifdef LOG_FLOW
    std::cout << "start assignInitialPath" << std::endl;
#endif
    int32_t l = inactivePathIndeces.top();
    inactivePathIndeces.pop();
    // Associate arrays with path index
    int32_t s = 0;
    for (int32_t lambda = 0; lambda <= m; ++lambda) {
        s = inactiveArrayIndeces[lambda].top();
        inactiveArrayIndeces[lambda].pop();
        pathIndexToArrayIndex[lambda][l] = s;
        arrayReferenceCount[lambda][s] = 1;
    }
#ifdef LOG_FLOW
    std::cout << "end assignInitialPath" << std::endl;
#endif
    return l;
}

int32_t SequentialDecoder::clonePath(int32_t l) {
#ifdef LOG_FLOW
    std::cout << "start clonePath" << std::endl;
#endif
    int32_t l1 = inactivePathIndeces.top();
    inactivePathIndeces.pop();
    // Make l1 reference same arrays as l
    int32_t s = 0;
    for (int32_t lambda = 0; lambda <= m; ++lambda) {
        s = pathIndexToArrayIndex[lambda][l];
        pathIndexToArrayIndex[lambda][l1] = s;
        ++arrayReferenceCount[lambda][s];
        p_layer_phases[l1*m + lambda] = p_layer_phases[l*m + lambda];
    }
#ifdef LOG_FLOW
    std::cout << "end clonePath" << std::endl;
#endif
    return l1;
}

void SequentialDecoder::killPath(int32_t l) {
#ifdef LOG_FLOW
    std::cout << "start KillPath " << l << std::endl;
#endif
    // Mark the path index l as inactive
    inactivePathIndeces.push(l);
    // Disassociate array with path index
    int32_t s = 0;
    for (int32_t lambda = 0; lambda <= m; ++lambda) {
        s = pathIndexToArrayIndex[lambda][l];
        --arrayReferenceCount[lambda][s];
        if (arrayReferenceCount[lambda][s] == 0) {
            inactiveArrayIndeces[lambda].push(s);
        }
    }
#ifdef LOG_FLOW
    std::cout << "end KillPath " << l << std::endl;
#endif
}

const double * SequentialDecoder::readPointer_D(int32_t lambda,
                                                int32_t l) {
#ifdef LOG_FLOW
    std::cout << "readPointer_D " << lambda << " " << l << std::endl;
#endif
    return const_cast<const double*>(D[lambda] +
                                     pathIndexToArrayIndex[lambda][l]);
}

const double * SequentialDecoder::readArrayPointer_S(int32_t lambda,
                                                     int32_t l) {
#ifdef LOG_FLOW
    std::cout << "readArrayPointer_S " << lambda
              << " " << pathIndexToArrayIndex[lambda][l]
              << std::endl;
#endif
    return const_cast<const double*>(
        arrayPointer_S[lambda][pathIndexToArrayIndex[lambda][l]]);
}

const bool ** SequentialDecoder::readArrayPointer_C(int32_t lambda,
                                                    int32_t l) {
#ifdef LOG_FLOW
    std::cout << "readArrayPointer_C " << lambda
              << " " << pathIndexToArrayIndex[lambda][l]
              << std::endl;
#endif
    return const_cast<const bool**>(
        arrayPointer_C[lambda][pathIndexToArrayIndex[lambda][l]]);
}

double * SequentialDecoder::getPointer_D(int32_t lambda, int32_t l) {
#ifdef LOG_FLOW
    std::cout << "getPointer_D " << lambda << " " << l << std::endl;
#endif
    int32_t s = pathIndexToArrayIndex[lambda][l];
    int32_t s1 = 0;
    if (arrayReferenceCount[lambda][s] == 1) {
        s1 = s;
    } else {
        s1 = inactiveArrayIndeces[lambda].top();
        inactiveArrayIndeces[lambda].pop();
        // copy all
        memcpy(arrayPointer_S[lambda][s1],
               arrayPointer_S[lambda][s],
               sizeof(double)*(1 << (m-lambda)));
        D[lambda][s1] = D[lambda][s];
        for (int32_t i = 0; i < (1 << (m - lambda)); ++i) {
            memcpy(arrayPointer_C[lambda][s1][i],
                   arrayPointer_C[lambda][s][i],
                   sizeof(bool)*2);
        }
        --arrayReferenceCount[lambda][s];
        arrayReferenceCount[lambda][s1] = 1;
        pathIndexToArrayIndex[lambda][l] = s1;
    }
    return D[lambda] + s1;
}

double * SequentialDecoder::getArrayPointer_S(int32_t lambda,
                                              int32_t l) {
#ifdef LOG_FLOW
    std::cout << "getArrayPointer_S " << lambda
              << " " << pathIndexToArrayIndex[lambda][l]
              << std::endl;
#endif
    int32_t s = pathIndexToArrayIndex[lambda][l];
    int32_t s1 = 0;
    if (arrayReferenceCount[lambda][s] == 1) {
        s1 = s;
    } else {
        s1 = inactiveArrayIndeces[lambda].top();
        inactiveArrayIndeces[lambda].pop();
        // copy all
        memcpy(arrayPointer_S[lambda][s1],
               arrayPointer_S[lambda][s],
               sizeof(double)*(1 << (m-lambda)));
        D[lambda][s1] = D[lambda][s];
        for (int32_t i = 0; i < (1 << (m - lambda)); ++i) {
            memcpy(arrayPointer_C[lambda][s1][i],
                   arrayPointer_C[lambda][s][i],
                   sizeof(bool)*2);
        }
        --arrayReferenceCount[lambda][s];
        arrayReferenceCount[lambda][s1] = 1;
        pathIndexToArrayIndex[lambda][l] = s1;
    }
    return arrayPointer_S[lambda][s1];
}

bool ** SequentialDecoder::getArrayPointer_C(int32_t lambda,
                                             int32_t l) {
#ifdef LOG_FLOW
    std::cout << "getArrayPointer_C " << lambda
              << " " << pathIndexToArrayIndex[lambda][l]
              << std::endl;
#endif
    int32_t s = pathIndexToArrayIndex[lambda][l];
    int32_t s1 = 0;
    if (arrayReferenceCount[lambda][s] == 1) {
        s1 = s;
    } else {
        s1 = inactiveArrayIndeces[lambda].top();
        inactiveArrayIndeces[lambda].pop();
        // copy all
        memcpy(arrayPointer_S[lambda][s1],
               arrayPointer_S[lambda][s],
               sizeof(double)*(1 << (m-lambda)));
        D[lambda][s1] = D[lambda][s];
        for (int32_t i = 0; i < (1 << (m - lambda)); ++i) {
            memcpy(arrayPointer_C[lambda][s1][i],
                   arrayPointer_C[lambda][s][i],
                   sizeof(bool)*2);
        }
        --arrayReferenceCount[lambda][s];
        arrayReferenceCount[lambda][s1] = 1;
        pathIndexToArrayIndex[lambda][l] = s1;
    }
    return arrayPointer_C[lambda][s1];
}

SequentialDecoder::SequentialDecoder(
    int32_t n,
    int32_t k,
    int32_t L,
    int32_t theta,
    double max_gap,
    double stddev,
    double *z,
    std::set<int32_t> *p_inf_constraints,
    int32_t *p_decision_points) :
    n(n),
    k(k),
    m(log2(n)),
    L(L),
    Theta(theta),
    stddev(stddev),
    max_gap(max_gap),
    inactivePathIndeces(theta),
    paths(theta),
    inactiveArrayIndeces(m+1, Stack<int32_t>(theta)),
    p_decision_points(p_decision_points)
{
    // Omega, fi, q,
    fi = new int32_t[Theta];
    q = new int32_t[n+1];

    int32_t frozen_symbols_count = n - k;
    pp_inf_constraints = new Triple*[frozen_symbols_count];
    p_inf_constraint_size = new int32_t[frozen_symbols_count];
    init_pp_inf_constraints(p_inf_constraints);

    O = new double[n];
    fillOmega(z, p_decision_points);

    // inactiveArrayIndeces, arrayPointer_C, arrayPointer_S,
    // arrayReferenceCount, D, pathIndexToArrayIndex
    D = new double*[m+1];
    arrayPointer_S = new double**[m+1];
    arrayPointer_C = new bool***[m+1];
    arrayReferenceCount = new int32_t*[m+1];
    pathIndexToArrayIndex = new int32_t*[m+1];
    for (int32_t lambda = 0; lambda <= m; ++lambda) {
        arrayPointer_S[lambda] = new double*[Theta];
        arrayPointer_C[lambda] = new bool**[Theta];
        for (int32_t l = 0; l < Theta; ++l) {
            int32_t size = 1 << (m-lambda);
            arrayPointer_S[lambda][l] = new double[size];
            arrayPointer_C[lambda][l] = new bool*[size];
            for (int32_t beta = 0; beta < size; ++beta) {
                arrayPointer_C[lambda][l][beta] = new bool[2];
            }
            inactiveArrayIndeces[lambda].push(l);
        }
        D[lambda] = new double[Theta];
        arrayReferenceCount[lambda] = new int32_t[Theta];
        pathIndexToArrayIndex[lambda] = new int32_t[Theta];
    }
    // inactivePathIndeces
    for (int32_t l = 0; l < Theta; ++l) {
        inactivePathIndeces.push(l);
    }
    // p_layer_phases
    p_layer_phases = new int32_t[Theta*(m+1)];
}

SequentialDecoder::~SequentialDecoder() {
    delete [] fi;
    delete [] q;
    delete [] O;
    for (int32_t lambda = 0; lambda <= m; ++lambda) {
        for (int l = 0; l < Theta; ++l) {
            int32_t size = 1 << (m-lambda);
            for (int beta = 0; beta < size; ++beta) {
                delete [] arrayPointer_C[lambda][l][beta];
            }
            delete [] arrayPointer_S[lambda][l];
            delete [] arrayPointer_C[lambda][l];
        }
        delete [] D[lambda];
        delete [] arrayPointer_S[lambda];
        delete [] arrayPointer_C[lambda];
        delete [] arrayReferenceCount[lambda];
        delete [] pathIndexToArrayIndex[lambda];
    }
    delete [] D;
    delete [] arrayPointer_S;
    delete [] arrayPointer_C;
    delete [] arrayReferenceCount;
    delete [] pathIndexToArrayIndex;
    for (int32_t i = 0; i < n - k; ++i) {
        if (p_inf_constraint_size[i] > 0) {
            delete [] pp_inf_constraints[i];
        }
    }
    delete [] pp_inf_constraints;
    delete [] p_inf_constraint_size;
    delete [] p_layer_phases;
}

void SequentialDecoder::clean_data_structures() {
    paths.clear();
    memset(fi, 0, sizeof(int32_t)*Theta);
    memset(q, 0, sizeof(int32_t)*(n+1));
    memset(p_layer_phases, ~0, sizeof(int32_t)*Theta*(m+1));
    for (int32_t lambda = 0; lambda <= m; ++lambda) {
        inactiveArrayIndeces[lambda].clear();
        for (int32_t l = 0; l < Theta; ++l) {
            inactiveArrayIndeces[lambda].push(l);
        }
        memset(arrayReferenceCount[lambda], 0, sizeof(int32_t)*Theta);
        memset(pathIndexToArrayIndex[lambda], 0, sizeof(int32_t)*Theta);
    }
    inactivePathIndeces.clear();
    for (int32_t l = 0; l < Theta; ++l)
        inactivePathIndeces.push(l);
    cmp_ops_count = 0;
    add_ops_count = 0;
    iteration = 0;
}

void SequentialDecoder::fillOmega(const double *z,
                                  const int32_t *p_decision_points) {
    O[n-1] = 0;
    for (int32_t fi = n-2; fi >= 0; --fi) {
        if (p_decision_points[fi+1] != (~0)) {
            O[fi] = O[fi+1] + log(1 - z[fi+1]);
        } else {
            O[fi] = O[fi+1];
        }
    }
}

inline tExpression operator+(const tExpression & A, const tExpression & B) {
    tExpression Res = A;
    tExpression::const_iterator it = B.begin();
    while (it != B.end()) {
        if (A.find(*it) != A.end()) {
            Res.erase(*it);
        } else {
            Res.insert(*it);
        }
        ++it;
    }
    return Res;
}

inline bool operator<(const Triple & A, const Triple & B) {
    if (A.lambda < B.lambda) {
        return true;
    }
    if (A.lambda == B.lambda) {
        if (A.beta < B.beta) {
            return true;
        }
        if ((A.beta == B.beta) && (A.phi < B.phi)) {
            return true;
        }
    }
    return false;
}

tExpression SequentialDecoder::RecoverInfSymbol(uint32_t lambda0,
                                                uint32_t lambda,
                                                uint32_t beta,
                                                uint32_t phi) {
    if (lambda == lambda0) {
        tExpression Res;
        Triple P;
        P.lambda = lambda;
        P.beta = beta;
        P.phi = phi & 1;
        Res.insert(P);
        return Res;
    } else {
        if ((phi & 1) == 1) {
            return RecoverInfSymbol(lambda0, lambda-1, 2*beta+1, phi/2);
        } else {
            return RecoverInfSymbol(lambda0, lambda-1, 2*beta+1, phi/2) +
                RecoverInfSymbol(lambda0, lambda-1, 2*beta, phi/2);
        }
    }
}

void SequentialDecoder::init_pp_inf_constraints(
    std::set<int32_t> *p_inf_constraints) {
    for (int32_t phi = 0; phi < n; ++phi) {
        if (p_decision_points[phi] == ~0) {
            continue;
        }
        int32_t i = p_decision_points[phi];
        std::set<Triple> Expr;

        for (int32_t j = 0; j < n; ++j) {
            if (j == phi) {
                continue;
            }
            if (p_inf_constraints[i].find(j) != p_inf_constraints[i].end()) {
                assert(phi > j);
                int32_t d = phi - j;
                int32_t lambda0 = m;
                while (d != 0) {
                    d >>= 1;
                    --lambda0;
                }
                // include m_pBitReversal[j] into the expression
                tExpression E = RecoverInfSymbol(lambda0+1, m, 0, j);
                Expr = Expr + E;
            }
        }
        p_inf_constraint_size[i] = Expr.size();
        if (p_inf_constraint_size[i] > 0) {
            pp_inf_constraints[i] = new Triple[Expr.size()];
            copy(Expr.begin(), Expr.end(), pp_inf_constraints[i]);
        } else {
            pp_inf_constraints[i] = nullptr;
        }
    }
}

void SequentialDecoder::erasePaths(int32_t min_fi) {
    auto it = paths.begin();
    auto end = paths.end();
    auto min1 = end;
    auto min2 = end;
    auto tmp = end;
    bool is_min1_set = false;
    bool is_min2_set = false;
    while (it != end) {
        int32_t l = it->second;
        if (fi[l] < min_fi) {
            killPath(l);
            paths.erase(&it);
        } else {
            if (!is_min1_set) {
                min1 = it;
                is_min1_set = true;
            } else if (!is_min2_set) {
                min2 = it;
                is_min2_set = true;
            }
            ++it;
        }
    }
    if (paths.size() == paths.capacity()) {
        killPath(min1->second);
        killPath(min2->second);
        paths.erase(&min1);
        paths.erase(&min2);
    } else if (paths.size() == paths.capacity()-1) {
        killPath(min1->second);
        paths.erase(&min1);
    }
}
