#ifndef _BINARY_HEAP_H_
#define _BINARY_HEAP_H_

#include <cassert>
#include <cstdint>

/* typedef int32_t T; */
template<class T, bool (*_cmp)(const T & a, const T & b)>
class BinaryHeap {
 private:
    const int32_t _capacity;
    int32_t _size;
    /* bool (*_cmp)(const T & a, const T & b); */
    T *_values;

 public:
    BinaryHeap(int32_t capacity): 
      _capacity(capacity), 
      _size(0)
    {
	_values = new T[_capacity];
    }
    ~BinaryHeap() {
	delete [] _values;
    }
    inline T min() const {
	assert(_size > 0);
	return *_values;
    }
    void pop_min() {
	assert(_size > 0);
	_values[0] = _values[_size-1];
	--_size;
	percolate_down(1);
    }
    void insert(const T & value) {
	assert(_size <= _capacity);
	_values[_size++] = value;
	percolate_up(_size);
    }
    inline bool empty() const { return _size == 0; }
    inline int32_t size() const { return _size; }
    inline int32_t capacity() const { return _capacity; }
    inline void clear() { _size = 0; }

    void print_values() {
	if (empty()) {
	    std::cout << "empty" << std::endl;
	} else {
	    for (int32_t i = 0; i < _size; ++i) {
		std::cout << _values[i] << " ";
	    }
	    std::cout << std::endl;
	}
    }

 private:
    void percolate_up(const int32_t & index) {
	int32_t i = index;
	while (i > 1 && !_cmp(_values[i/2-1], _values[i-1])) {
	    T tmp = _values[i/2-1];
	    _values[i/2-1] = _values[i-1];
	    _values[i-1] = tmp;
	    i /= 2;
	}
    }

    void percolate_down(const int32_t & index) {
	int32_t i = index;
	while (2*i <= _size) {
	    int32_t j = 2*i;
	    if (j < _size && !_cmp(_values[j-1], _values[j])) ++j;
	    if (_cmp(_values[i-1], _values[j-1])) break;
	    T tmp = _values[i-1];
	    _values[i-1] = _values[j-1];
	    _values[j-1] = tmp;
	    i = j;
	}
    }

};

#endif /* _BINARY_HEAP_H_ */
