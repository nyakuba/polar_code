#ifndef _BINOMIAL_HEAP_H_
#define _BINOMIAL_HEAP_H_

#include <iostream>
#include <cstdint>
#include <cassert>

typedef int32_t T;
class BinomialHeap {
 private:
    class HeapNode {
     public:
	T _value;
	HeapNode *_right;
	HeapNode *_parent;
	HeapNode *_child;
	int32_t _degree;
        HeapNode(T value):
	_value(value),
	_right(nullptr), 
	_parent(nullptr), 
	_child(nullptr), 
	_degree(0) 
	{ }
        HeapNode(const HeapNode & node):
	_value(node._value),
	_right(node._right), 
	_parent(node._parent), 
	_child(node._child), 
	_degree(node._degree) 
	{ }
	~HeapNode() { }
    };
    int32_t _size;
    HeapNode *_head;

 public:
    BinomialHeap(): _size(0), _head(nullptr) {}
    BinomialHeap(const BinomialHeap & heap): _size(heap._size), _head(heap._head) {}
    ~BinomialHeap() { /*delete_heap(_head);*/ }

    void delete_heap(HeapNode * node) {
	std::cout << "delete" << std::endl;
	while (node != nullptr) {
	    delete_heap(node->_child);
	    HeapNode * n = node;
	    node = node->_right;
	    std::cout << "deleting " << node->_value << std::endl;
	    delete n;
	}
    }

    T min() {
	assert(_size > 0);
	HeapNode * min_node = _head;
	HeapNode * node = _head;
	while (node->_right != nullptr) {
	    node = node->_right;
	    if (_cmp(node->_value, min_node->_value)) {
		min_node = node;
	    }
	}
	return min_node->_value;
    }

    void insert(const T & value) {
	HeapNode *node = new HeapNode(value);
	BinomialHeap heap;
	heap._head = node;
	*this = binomial_heap_union(*this, heap);
	++_size;
    }

    T pop_min() {
	assert(_size > 0);
	HeapNode *min_node = _head;
	HeapNode *prev_min_node = nullptr;
	HeapNode *node = _head->_right;
	HeapNode *prev_node = _head;
	while (node != nullptr) {
	    if (_cmp(node->_value, min_node->_value)) {
		min_node = node;
		prev_min_node = prev_node;
	    }
	    prev_node = node;
	    node = node->_right;
	}
	// delete min_node from list
	if (prev_min_node == nullptr) {
	    _head = min_node->_right;
	} else {
	    prev_min_node = min_node->_right;
	}

	// reverse list of childs
	if (min_node->_child != nullptr) {
	    BinomialHeap H;
	    H._head = min_node->_child;
	    H._head->_right = nullptr;
	    prev_node = min_node->_child->_right;
	    node = prev_node;
	    while (node != nullptr) {
		node = node->_right;
		prev_node->_right = H._head;
		H._head = prev_node;
		prev_node = node;
	    }
	    *this = binomial_heap_union(*this, H);
	}
	
	--_size;
	T value = min_node->_value;
	delete min_node;
	return value;
    }

 private:
    // make one heap from two heaps a and b.
    // a -> child of b.
    void binomial_heap_link(HeapNode *a, HeapNode *b) {
	a->_parent = b;
	a->_right = b->_child;
	b->_child = a;
	++b->_degree;
    }

    BinomialHeap binomial_heap_merge(BinomialHeap & H1, BinomialHeap & H2) {
	if (H1._head == nullptr) return H2;
	if (H2._head == nullptr) return H1;
	BinomialHeap H;
	if (H1._head->_degree < H2._head->_degree) {
	    H._head = H1._head;
	    H1._head = H1._head->_right;
	} else {
	    H._head = H2._head;
	    H2._head = H2._head->_right;
	}
	HeapNode *tail = H._head;
	while (H1._head != nullptr && H2._head != nullptr) {
	    if (H1._head->_degree < H2._head->_degree) {
		tail->_right = H1._head;
		H1._head = H1._head->_right;
	    } else {
		tail->_right = H2._head;
		H2._head = H2._head->_right;
	    }
	}
	if (H1._head == nullptr) {
	    tail->_right = H2._head;
	}
	if (H2._head == nullptr) {
	    tail->_right = H1._head;
	}
	H._size = H1._size + H2._size;
	return H;
    }

    BinomialHeap binomial_heap_union(BinomialHeap & H1, BinomialHeap & H2) {
	BinomialHeap H = binomial_heap_merge(H1, H2);
	/* H1._head = nullptr; */
	/* H2._head = nullptr; */
	if (H._head == nullptr) return H;
	HeapNode *prev_x = nullptr;
	HeapNode *x = H._head;
	HeapNode *next_x = x->_right;
	while (next_x != nullptr) {
	    if ((x->_degree != next_x->_degree) 
		|| ((next_x->_right != nullptr) 
		    && (next_x->_right->_degree == x->_degree))) {
		prev_x = x;
		x = next_x;
	    } else if (_cmp(x->_value, next_x->_value)) {
		x->_right = next_x->_right;
		binomial_heap_link(next_x, x);
	    } else {
		if (prev_x == nullptr) {
		    H._head = next_x;
		} else {
		    prev_x->_right = next_x;
		}
		binomial_heap_link(x, next_x);
		x = next_x;
	    }
	    next_x = x->_right;
	}
	H._size = H1._size + H2._size;
	return H;
    }
    
    bool _cmp(const T & a, const T & b) const {
	return a < b;
    }
};

#endif /* _BINOMIAL_HEAP_H_ */
