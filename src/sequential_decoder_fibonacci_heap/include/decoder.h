#ifndef _SEQUENTIAL_DECODER_H_
#define _SEQUENTIAL_DECODER_H_

#include <vector>
#include <set>
#include "common.h"
#include "stack.h"
#include "code.h"
#include "fibonacci_heap.h"

typedef typename std::pair<double, int32_t> PAIR;

class SequentialDecoder {
 private:
    // operation counters
    static int64_t cmp_ops_count;
    static int64_t add_ops_count;

    static inline double plus(const double & a, const double & b) {
	++add_ops_count;
	return a + b;
    }
    static inline void add(double & a, const double & b) {
	++add_ops_count;
	a += b;
    }
    static inline bool bxor(const bool & a, const bool & b) {
	return a != b;
    }
    static inline bool compare(const PAIR & a, const PAIR & b) {
	++cmp_ops_count;
	if (a.first == b.first) 
	    return a.second > b.second;
	return a.first > b.first;
    }

    const int32_t n, k, m, L, Theta;
    bool *frozen;
    bool ****arrayPointer_C;
    int32_t *fi;
    int32_t *q;
    int32_t **arrayReferenceCount;
    int32_t **pathIndexToArrayIndex;
    double stddev;
    double *O; // Omega: O[i] = product (1 - P_j), j > i, j in F
    double **D;
    double ***arrayPointer_S;
    Stack<int32_t> inactivePathIndeces;
    FibonacciHeap<PAIR, &SequentialDecoder::compare> paths;
    std::vector<Stack<int32_t> > inactiveArrayIndeces;

 public:
    SequentialDecoder(int32_t n, int32_t k, int32_t L, int32_t theta,
		      double stddev, int32_t *indeces, double *O);
    ~SequentialDecoder();
 
    ///@param received vector to decode
    ///@return decoded codeword
    void decode(double *y, bool *c);

    int64_t get_cmp_ops_count() {
	return cmp_ops_count;
    }
    int64_t get_add_ops_count() {
	return add_ops_count;
    }
        clock_t get_heap_total_time() {
            return 0;
        }
 
 private:
    ///@return index l of initial path
    int32_t assignInitialPath();

    ///@param index l of path to clone
    ///@return index l' to copy
    int32_t clonePath(int32_t l);

    ///@param index l of path to kill
    void killPath(int32_t l);

    // returns private copy of ArrayPointer_P if ReferenceCount > 1
    ///@param layer lambda
    ///@param path index l
    ///@return pointer to corresponding probability pair array
    double *getArrayPointer_S(int32_t lambda, int32_t l);

    // returns private copy of ArrayPointer_C if ReferenceCount > 1
    ///@param layer lambda
    ///@param path index l
    ///@return pointer to corresponding bit pair array
    bool **getArrayPointer_C(int32_t lambda, int32_t l);

    // returns private copy of D if ReferenceCount > 1
    ///@param layer lambda
    ///@param path index l
    ///@return pointer to corresponding D
    double *getPointer_D(int32_t lambda, int32_t l);

    // no private copy
    ///@param layer lambda
    ///@param path index l
    ///@return pointer to corresponding bit pair array
    const double *readArrayPointer_S(int32_t lambda, int32_t l);

    // no private copy
    ///@param layer lambda
    ///@param path index l
    ///@return pointer to corresponding bit pair array
    const bool **readArrayPointer_C(int32_t lambda, int32_t l);

    // no private copy
    ///@param layer lambda
    ///@param path index l
    ///@return pointer to corresponding D
    const double *readPointer_D(int32_t lambda, int32_t l);

    ///@param path l
    ///@param layer lambda
    ///@param phase fi
    void recursivelyCalcS(int32_t l, int32_t lambda, int32_t fi);

    ///@param path l
    ///@param layer lambda
    ///@param phase fi
    void recursivelyUpdateC(int32_t l, int32_t lambda, int32_t fi);

    /// Erases paths with fi < min_fi
    void erasePaths(int32_t min_fi);

    void fillOmega(const double *z, const bool *frozen);

    inline double W(const double &y, const double &x) const {
	return 1 / (1 + exp((-1)*x*2 * y / (stddev*stddev)));
    }
    inline double initS(const double &y) const {
	return -2 * y / (stddev*stddev);
    }
    inline double Q(const double & a, const double & b) {
	return sign(a)*sign(b)*std::min(std::abs(a), std::abs(b));
    }
    template <class T> 
    inline int32_t sign(T val) {
	return (T(0) < val) - (val < T(0));
    }
    void clean_data_structures();
};


#endif /* _SEQUENTIAL_DECODER_H_ */
