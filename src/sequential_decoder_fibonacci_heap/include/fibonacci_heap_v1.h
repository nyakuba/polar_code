#ifndef _FIBONACCI_HEAP_H_
#define _FIBONACCI_HEAP_H_

#include <iostream>
#include <cassert>
#include <cmath>
#include "stack.h"

template<class T, bool (*_cmp)(const T & a, const T & b)>
class FibonacciHeap {
 private:
    Stack<int32_t> free_indeces;
    T * keys;
    int32_t * left;
    int32_t * right;
    int32_t * parent;
    int32_t * child;
    int32_t * degree;
    bool * mark;
    int32_t min;
    int32_t root_list_size;
    int32_t _size;
    const int32_t _capacity;
    // temporary array
    const int32_t a_size;
    int32_t * a;
    
 public:
    // constructors
    FibonacciHeap(int32_t capacity):
        free_indeces(capacity),
	min(-1), root_list_size(0), 
	_size(0), _capacity(capacity), 
	a_size((int32_t)log2(_capacity)+1)
	    {
		for (int32_t i = 0; i < _capacity; ++i) {
		    free_indeces.push(i);
		}
		keys = new T[_capacity];
		left = new int32_t[_capacity];
		right = new int32_t[_capacity];
		parent = new int32_t[_capacity];
		child = new int32_t[_capacity];
		degree = new int32_t[_capacity];
		mark = new bool[_capacity];
		a = new int32_t[a_size];
	    }
    ~FibonacciHeap() {
	delete [] keys;
	delete [] left;
	delete [] right;
	delete [] parent;
	delete [] child;
	delete [] degree;
	delete [] mark;
    }
    
    int32_t size() const {
	return _size;
    }

    int32_t capacity() const {
	return _capacity;
    }

    T top() const {
	return keys[min];
    }

    void push(const T & key) {
	assert(_size < _capacity);
	int32_t x = free_indeces.top();
	free_indeces.pop();
	keys[x] = key;
	mark[x] = false;
	degree[x] = 0;
	parent[x] = -1;
	child[x] = -1;
	insert_root(x);
	++_size;
    }

    void pop() {
	if (_size == 1) {
	    min = -1;
	    --root_list_size;
	    return;
	}
	// set min's childs parent to -1
	if (child[min] != -1) {
	    int childs_count = 1;
	    parent[child[min]] = -1;
	    int32_t c = right[child[min]];
	    while (parent[c] != -1) {
		parent[c] = -1;
		c = right[c];
		++childs_count;
	    }
	    // add min childs to heap root list
	    right[left[min]] = child[min];
	    left[min] = left[child[min]];
	    right[left[child[min]]] = min;
	    left[child[min]] = left[min];
	    root_list_size += childs_count;
	}
	// remove min from root list
	right[left[min]] = right[min];
	left[right[min]] = left[min];
	--root_list_size;
	// free min
	free_indeces.push(min);
	min = right[min];

	consolidate();

	--_size;
    }

    void print_heap() {
	if (min == -1) {
	    std::cout << "empty heap" << std::endl;
	    return;
	}
	int32_t root = min;
	do {
	    std::cout << "root node " << root << ":" << keys[root] << ": degree " << degree[root] << std::endl;
	    print_tree(root);
	    root = right[root];
	} while (root != min);
    }

    void del(int32_t x) {
	erase(x);
    }

    void decr(int32_t x, int32_t key) {
	decrease_key(x, key);
    }

    void clear() {
	min = -1;
	_size = 0;
	root_list_size = 0;
	free_indeces.clear();
	for (int32_t i = 0; i < _capacity; ++i) {
	    free_indeces.push(i);
	}
    }

 private:
    void insert_root(int32_t x) {
	if (min == -1) {
	    left[x] = x;
	    right[x] = x;
	    min = x;
	} else {
	    right[left[min]] = x;
	    left[x] = left[min];
	    right[x] = min;
	    left[min] = x;
	    if (_cmp(keys[x], keys[min])) {
		min = x;
	    }
	}
	++root_list_size;
    }

    void consolidate() {
	// init temporary array a
	for (int32_t i = 0, *pa = a; i < a_size; ++i, ++pa) {
	    *pa = -1;
	}

	int32_t x = min;
	for (int i = 0; i < root_list_size; ++i) {
	    int32_t rx = right[x];
	    int32_t d = degree[x];
	    while (a[d] != -1) {
		int32_t y = a[d];
		if (_cmp(keys[y], keys[x])) {
		    int32_t tmp = x; x = y; y = tmp;
		}
		fib_heap_link(y, x);
		a[d] = -1;
		++d;
	    }
	    a[d] = x;
	    x = rx;
	}
	
	min = -1;
	root_list_size = 0;
	for (int32_t i = 0, *pa = a; i < a_size; ++i, ++pa) {
	    if (*pa != -1) {
		insert_root(*pa);
	    }
	}
    }

    void fib_heap_link(int32_t y, int32_t x) {
	assert(x != y);
	// make y x child
	parent[y] = x;
	mark[y] = false;
	if (child[x] == -1) {
	    child[x] = y;
	    right[y] = y;
	    left[y] = y;
	} else {
	    right[left[child[x]]] = y;
	    left[y] = left[child[x]];
	    right[y] = child[x];
	    left[child[x]] = y;
	}
	++degree[x];
    }

    void decrease_key(int32_t x, int32_t key) {
	assert(_cmp(key, keys[x]));
	keys[x] = key;
	int32_t y = parent[x];
	if (y != -1 && _cmp(keys[x], keys[y])) {
	    cut(x, y);
	    cascading_cut(y);
	}
	if (_cmp(keys[x], keys[min])) 
	    min = x;
    }

    void cut(int32_t x, int32_t y) {
	if (child[y] == x) {
	    if (degree[y] == 1) {
		child[y] = -1;
	    } else {
		child[y] = right[x];
		left[right[x]] = left[x];
		right[left[x]] = right[x];
	    }
	} else {
	    left[right[x]] = left[x];
	    right[left[x]] = right[x];
	}
	--degree[y];
	parent[x] = -1;
	mark[x] = false;
	insert_root(x);
    }

    void cascading_cut(int32_t y) {
	int32_t z = parent[y];
	if (z != -1) {
	    if (!mark[y]) {
		mark[y] = true;
	    } else {
		cut(y, z);
		cascading_cut(z);
	    }
	}
    }

    void erase(int32_t x) {
	if (x == min) {
	    pop();
	} else {
	    int32_t y = parent[x];
	    if (y != -1) {
		cut(x, y);
		cascading_cut(y);
	    }
	    // add x childs to root list
	    if (child[x] != -1) {
		int childs_count = 1;
		parent[child[x]] = -1;
		int32_t c = right[child[x]];
		while (parent[c] != -1) {
		    parent[c] = -1;
		    c = right[c];
		    ++childs_count;
		}
		// add min childs to heap root list
		right[left[min]] = child[x];
		left[min] = left[child[x]];
		right[left[child[x]]] = min;
		left[child[x]] = left[min];
		root_list_size += childs_count;
	    }
	    // remove and free x
	    right[left[x]] = right[x];
	    left[right[x]] = left[x];
	    free_indeces.push(x);
	    --root_list_size;
	}
    }

    void print_tree(int32_t x) const {
	if (child[x] == -1) {
	    std::cout << "no childs" << std::endl;
	    return;
	}
	int32_t c = child[x];
	do {
	    std::cout << "child node " << c << ":" << keys[c] << ": parent " << parent[c] << ": degree " << degree[c] << std::endl;
	    print_tree(c);
	    c = right[c];
	} while (c != child[x]);
    }
};

#endif /* _FIBONACCI_HEAP_H_ */
