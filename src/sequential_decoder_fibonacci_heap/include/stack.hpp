#ifndef _STACK_HPP_
#define _STACK_HPP_

#include "stack.h"

#ifdef STACK_LOG
template<class T>
int32_t Stack<T>::sid = 0;
#endif

template<class T>
Stack<T> & Stack<T>::operator=(const Stack<T> & stack) {
    if (this == &stack) return;
    if (_capacity != stack._capacity) {
	delete [] _values;
	_capacity = stack._capacity;
	_values = new T[_capacity];
    }
    memcpy(_values, stack._values, sizeof(T)*_capacity);
    _size = stack._size;
    _top = _values + _size;    
}

template<class T>
void Stack<T>::pop() {
#ifdef STACK_LOG
    std::cout << "stack " << id << " pop" << std::endl;
#endif
    assert(_size > 0);
    --_top; --_size;
}

template<class T>
T & Stack<T>::top() const {
#ifdef STACK_LOG
    std::cout << "stack " << id << " top = " << *(_top-1) << std::endl;
#endif
    assert(_size > 0);
    return *(_top-1);
}

template<class T>
void Stack<T>::push(T & value) {
#ifdef STACK_LOG
    std::cout << "stack " << id << " push" << std::endl;
#endif
    assert(_size < _capacity);
    *_top = value;
    ++_top; ++_size;
}

template<class T>
int32_t Stack<T>::size() const {
    return _size;
}
template<class T>
int32_t Stack<T>::capacity() const {
    return _capacity;
}

template<class T>
Stack<T>::Stack(int32_t capacity): _capacity(capacity), _size(0) {
#ifdef STACK_LOG
    id = sid++;
#endif
    _values = new T[_capacity];
    _top = _values;
}

template<class T>
Stack<T>::Stack(const Stack<T> & stack): 
    _capacity(stack._capacity), _size(stack._size) {
#ifdef STACK_LOG
    id = sid++;
#endif
    _values = new T[_capacity];
    memcpy(_values, stack._values, sizeof(T)*_capacity);
    _size = stack._size;
    _top = _values + _size;
}

template<class T>
Stack<T>::~Stack() {
#ifdef STACK_LOG
    std::cout << "~Stack" << std::endl;
#endif
    delete [] _values;
}

template<class T>
void Stack<T>::clear() {
    _size = 0;
    _top = _values;
}

#endif /* _STACK_HPP_ */
