#ifndef _FIBONACCI_HEAP_H_
#define _FIBONACCI_HEAP_H_

#include <iostream>
#include <cassert>
#include <cmath>
#include <iterator>
#include "stack.h"

template<class T, bool (*_cmp)(const T & a, const T & b)>
class FibonacciHeap {
 public:
    typedef int32_t size_t;
    typedef size_t index_t;
    typedef int32_t degree_t;
    typedef T* pointer;
    typedef T& reference;
    typedef const T* const_pointer;
    typedef const T& const_reference;
    class const_iterator;

 private:
    const size_t _capacity;
    T * keys;
    size_t _size;
    index_t min;
    size_t root_list_size;
    index_t * left;
    index_t * parent;
    index_t * child;
    degree_t * degree;
    Stack<int32_t> free_indeces;

    const size_t a_size;    // temporary array
    index_t * a;
    
 public:

    // constructors
    FibonacciHeap(size_t capacity):
        _capacity(capacity), 
        _size(0), 
	min(-1),
        root_list_size(0), 
        free_indeces(capacity),
	// size should be log2(_capacity)+1 and 1 element
	// determines end of the array
	a_size((size_t)log2(_capacity)+2) 
	    {
		keys = new T[_capacity];
		left = new int32_t[_capacity];
		parent = new int32_t[_capacity];
		child = new int32_t[_capacity];
		degree = new int32_t[_capacity];
		for (index_t i = 0; i < _capacity; ++i) {
		    free_indeces.push(i);
		    degree[i] = -1;
		}
		a = new int32_t[a_size];
	    }

    ~FibonacciHeap() {
	delete [] keys;
	delete [] left;
	delete [] parent;
	delete [] child;
	delete [] degree;
    }
    
    size_t size() const {
	return _size;
    }

    bool empty() const {
	return _size == 0;
    }

    size_t capacity() const {
	return _capacity;
    }

    const_reference top() const {
	assert(_size > 0);
	return keys[min];
    }

    void push(const_reference key) {
	assert(_size < _capacity);
	index_t x = free_indeces.top();
	free_indeces.pop();
	keys[x] = key; 
	degree[x] = 0; // degree = 0, mark = false;
	parent[x] = -1;
	child[x] = -1;
	insert_root(x);
	++_size;
    }

    void pop() {
	assert(_size > 0);
	if (_size == 1) {
	    degree[min] = -1;
	    free_indeces.push(min);
	    min = -1;
	    --root_list_size;
	    --_size;
	    return;
	}
	// set min's childs parent to -1
	if (child[min] != -1) {
	    int32_t childs_count = 1;
	    parent[child[min]] = -1;
	    index_t c = left[child[min]];
	    while (parent[c] != -1) {
		parent[c] = -1;
		c = left[c];
		++childs_count;
	    }
	    // add min childs to heap root list
	    index_t left_min = left[min];
	    left[min] = left[child[min]];
	    left[child[min]] = left_min;
	    root_list_size += childs_count;
	}
	// remove min from root list
	// set left[min] parents to min
	if (child[left[min]] != -1) {
	    parent[child[left[min]]] = min;
	    index_t c = left[child[left[min]]];
	    while (parent[c] != min) {
		parent[c] = min;
		c = left[c];
	    }
	}
	// swap min and left[min]
	swap(keys[min], keys[left[min]]);
	swap(degree[min], degree[left[min]]);
	swap(child[min], child[left[min]]);
	// remove left[min]
	degree[left[min]] = -1;
	free_indeces.push(left[min]);
	left[min] = left[left[min]];
	--root_list_size;

	consolidate();

	--_size;
    }

    void decrease_key(index_t x, const_reference key) {
	assert(_cmp(key, keys[x]));
	keys[x] = key;
	index_t y = parent[x];
	if (y != -1 && _cmp(keys[x], keys[y])) {
	    cut(x, y);
	    cascading_cut(y);
	}
	if (_cmp(keys[x], keys[min])) 
	    min = x;
    }

    // erases element end increments iterator
    void erase(const_iterator & it) {
	index_t x = it.current_index;
	/* std::cout << x << std::endl; */
	index_t lx = left[x];
	// here will be swap(x, lx)
	erase(x);
	// needed by for loop
	// lx < x means, that we may already has checked x before
	if (lx <= it.current_index) {
	    ++it;
	}
    }

    void erase(index_t x) {
	if (x == min) {
	    pop();
	} else {
	    index_t y = parent[x];
	    if (y != -1) {
		cut(x, y);
		cascading_cut(y);
	    }
	    // add x childs to root list
	    if (child[x] != -1) {
		int32_t childs_count = 1;
		parent[child[x]] = -1;
		index_t c = left[child[x]];
		while (parent[c] != -1) {
		    parent[c] = -1;
		    c = left[c];
		    ++childs_count;
		}
		// add x childs to heap root list
		index_t left_min = left[min];
		left[min] = left[child[x]];
		left[child[x]] = left_min;
		root_list_size += childs_count;
	    }
	    // remove and free x
	    if (child[left[x]] != -1) {
		parent[child[left[x]]] = x;
		index_t c = left[child[left[x]]];
		while (parent[c] != x) {
		    parent[c] = x;
		    c = left[c];
		}
	    }
	    swap(keys[x], keys[left[x]]);
	    swap(degree[x], degree[left[x]]);
	    swap(child[x], child[left[x]]);
	    free_indeces.push(left[x]);
	    degree[left[x]] = -1;
	    if (min == left[x]) {
		min = x;
	    }
	    left[x] = left[left[x]];
	    --root_list_size;
	    --_size;
	}
    }

    void clear() {
	min = -1;
	_size = 0;
	root_list_size = 0;
	free_indeces.clear();
	for (index_t i = 0; i < _capacity; ++i) {
	    free_indeces.push(i);
	}
    }

    void print_degrees() {
	for (index_t i = 0; i < _capacity; ++i) {
	    std::cout << (degree[i] >> 1) << " ";
	}
	std::cout << std::endl;
    }

    void print_marks() {
	for (index_t i = 0; i < _capacity; ++i) {
	    if (degree[i] >= 0) {
		std::cout << (degree[i] & 1) << " ";
	    } else {
		std::cout << -1 << " ";
	    }
	}
	std::cout << std::endl;
    }

 private:
    void insert_root(const index_t & x) {
	if (min == -1) {
	    left[x] = x;
	    min = x;
	} else {
	    left[x] = left[min];
	    left[min] = x;
	    if (_cmp(keys[x], keys[min])) {
		min = x;
	    }
	}
	++root_list_size;
    }

    void consolidate() {
	// init temporary array a
	for (index_t *pa = a; pa < (a + a_size); ++pa) {
	    *pa = -1;
	}

	index_t x = min;
	for (index_t i = 0; i < root_list_size; ++i) {
	    index_t lx = left[x];
	    index_t d = (degree[x] >> 1);
	    while (a[d] != -1) {
		assert(d < a_size);
		index_t y = a[d];
		if (_cmp(keys[y], keys[x])) {
		    index_t tmp = x; x = y; y = tmp;
		}
		fib_heap_link(y, x);
		a[d] = -1;
		++d;
	    }
	    a[d] = x;
	    x = lx;
	}
	
	min = -1;
	root_list_size = 0;
	for (index_t i = 0, *pa = a; i < a_size; ++i, ++pa) {
	    if (*pa != -1) {
		insert_root(*pa);
	    }
	}
    }

    void fib_heap_link(const index_t & y, const index_t & x) {
	assert(x != y);
	// make y x child
	parent[y] = x;
	degree[y] = (degree[x] >> 1) << 1; // mark[y] = false;
	if (child[x] == -1) {
	    child[x] = y;
	    left[y] = y;
	} else {
	    left[y] = left[child[x]];
	    left[child[x]] = y;
	}
	degree[x] += 2;
    }

    void cut(index_t & x, const index_t & y) {
	if ((degree[y] >> 1) == 1) {
	    child[y] = -1;
	} else {
	    // maybe it is more efficient to walk through 
	    // the child's list in order to find right[x]?
	    if (child[x] != -1) {
		parent[child[x]] = left[x];
		int32_t c = left[child[x]];
		while (parent[c] != left[x]) {
		    parent[c] = left[x];
		    c = left[c];
		}
	    }
	    if (child[left[x]] != -1) {
		parent[child[left[x]]] = x;
		int32_t c = left[child[left[x]]];
		while (parent[c] != x) {
		    parent[c] = x;
		    c = left[c];
		}
	    }
	    swap(keys[x], keys[left[x]]);
	    swap(degree[x], degree[left[x]]);
	    swap(child[x], child[left[x]]);
	    int32_t tmp = left[x];
	    if (child[parent[x]] == left[x])
		child[parent[x]] = x;
	    left[x] = left[left[x]];
	    x = tmp;
	}
	degree[y] -= 2;
	parent[x] = -1;
	degree[x] = (degree[x] >> 1) << 1; // mark[x] = false;
	insert_root(x);
    }

    void cascading_cut(index_t & y) {
	int32_t z = parent[y];
	if (z != -1) {
	    if ((degree[y] & 1) == 0) { // if mark[y] == false
		degree[y] |= 1;         // mark[y] = true;
	    } else {
		cut(y, z);
		cascading_cut(z);
	    }
	}
    }

    template<class Ty>
    void swap(Ty & a, Ty & b) {
	Ty tmp = a;
	a = b;
	b = tmp;
    }

 public:
    class const_iterator {
	friend class FibonacciHeap;
    public:
	typedef typename FibonacciHeap::size_t size_t;
	typedef typename FibonacciHeap::index_t index_t;
	typedef typename FibonacciHeap::const_pointer const_pointer;
	typedef typename FibonacciHeap::const_reference const_reference;
    private:
	const FibonacciHeap * heap;
	index_t current_index;

        const_iterator(const FibonacciHeap * heap, index_t index):
	heap(heap),
	current_index(index)
	{ }
    public:
        const_iterator(const const_iterator & it): 
	heap(it.heap),
	current_index(it.current_index)
	{ }
	~const_iterator() { }
	
	const_iterator & operator=(const const_iterator & it) {
	    this->current_index = it.current_index;
	    return *this;
	}
	bool operator==(const const_iterator & it) const {
	    if (this->heap == it.heap) {
		return this->current_index == it.current_index;
	    } else {
		return false;
	    }
	}
	bool operator!=(const const_iterator & it) const {
	    if (this->heap == it.heap) {
		return this->current_index != it.current_index;
	    } else {
		return true;
	    }
	}
	    
	const_iterator & operator++() {
	    if (current_index < heap->_capacity) {
		do {
		    ++current_index;
		} while (current_index < heap->_capacity 
			 && heap->degree[current_index] == -1);
	    }
	    return *this;
	}
	const_iterator & operator++(int) {
	    const_iterator tmp = *this;
	    ++(*this);
	    return tmp;
	}
	
	const_reference operator*() const {
	    return heap->keys[current_index];
	}
	const_pointer operator->() const {
	    return heap->keys + current_index;
	}
    };

    const_iterator begin() const {
	const_iterator it(this, 0);
	if (degree[0] == -1) {
	    ++it;
	}
	return it;
    }
    const_iterator end() const {
	return const_iterator(this, _capacity);
    }

 /* public: */
 /*    void print_heap() { */
 /*    	if (min == -1) { */
 /*    	    std::cout << "empty heap" << std::endl; */
 /*    	    return; */
 /*    	} */
 /*    	std::cout << "heap size: " << _size << std::endl; */
 /*    	int32_t root = min; */
 /*    	do { */
 /*    	    std::cout << "root node " << root << ":" << keys[root] << ": degree " << degree[root] << std::endl; */
 /*    	    print_tree(root); */
 /*    	    root = left[root]; */
 /*    	} while (root != min); */
 /*    } */

 /*    void print_tree(int32_t x) const { */
 /*    	if (child[x] == -1) { */
 /*    	    std::cout << "no childs" << std::endl; */
 /*    	    return; */
 /*    	} */
 /*    	int32_t c = child[x]; */
 /*    	do { */
 /*    	    std::cout << "child node " << c << ":" << keys[c] << ": parent " << parent[c] << ": degree " << degree[c] << std::endl; */
 /*    	    print_tree(c); */
 /*    	    c = left[c]; */
 /*    	} while (c != child[x]); */
 /*    } */
};

#endif /* _FIBONACCI_HEAP_H_ */
