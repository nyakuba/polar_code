#include "include/decoder.h"

// #define LOG
// #define LOG_P

Decoder::Decoder(int32_t n, int32_t k, int32_t *ind, int32_t *shuf, double sd):
  m(log2(n)), N(n), K(k), stddev(sd), indeces(ind), shuffle(shuf)
{
  // std::cout << "m = " << m << std::endl;

  P = new double**[m+1];
  C = new bool**[m+1];
  for (int i = 0; i <= m; ++i) {
    P[i] = new double*[N];
    C[i] = new bool*[N];
    for (int j = 0; j < N; ++j) {
      P[i][j] = new double[2];
      C[i][j] = new bool[2];
      memset(P[i][j], 0, sizeof(double)*2);
      memset(C[i][j], 0, sizeof(bool)*2);
    }
  }
  frozen = new bool[N];
  memset(frozen, 0, sizeof(bool)*N);
  for (int i = N-1; i >= K; --i) {
    // std::cout << indeces[i] << " " << shuffle[indeces[i]] << " " << indeces[shuffle[i]] << std::endl;
    frozen[indeces[i]] = true;
  }
  // std::cout << "frozen positions: ";
  // for (int i = 0; i < N; ++i) {
  //   std::cout << frozen[i] << " ";
  // }
  // std::cout << std::endl;
}

Decoder::~Decoder() {
  delete [] frozen;
  for (int i = 0; i <= m; ++i) {
    for (int j = 0; j < N; ++j) {
      delete [] P[i][j];
      delete [] C[i][j];
    }
    delete [] P[i];
    delete [] C[i];
  }
  delete [] P;
  delete [] C;
}

void Decoder::decode(double *y, bool *c) {
  for (int beta = 0; beta < N; ++beta) {
    P[0][beta][0] = W(y[beta], -1);
    P[0][beta][1] = W(y[beta], 1);
  }
    
  for (int fi = 0; fi < N; ++fi) {
    recursivelyCalcP(m, fi);
    int32_t fm2 = fi%2;

#ifdef LOG_P
    for (int i = 0; i <= m; ++i) {
      std::cout << "P[" << i << "][.][0] : ";
      for (int j = 0; j < (1 << (m-i)); ++j)
	std::cout << P[i][j][0] << " ";
      std::cout << std::endl;
      std::cout << "P[" << i << "][.][1] : ";
      for (int j = 0; j < (1 << (m-i)); ++j)
	std::cout << P[i][j][1] << " ";
      std::cout << std::endl;
    }
#endif
#ifdef LOG
    std::cout << "fi " << fi << std::endl;
    std::cout << "CHOOSING BIT: " << P[m][0][0] << " vs " <<  P[m][0][1] << std::endl;
    if (frozen[fi]) {
      std::cout << "FROZEN BIT!!!" << std::endl;
      if (P[m][0][0] < P[m][0][1])
      	std::cout << "FROZEN FAIL!!!" << std::endl;
      C[m][0][fm2] = false;
    } else {
      if (P[m][0][0] > P[m][0][1]) {
	std::cout << "CHOOSE ZERO!!!" << std::endl;
	C[m][0][fm2] = false;
      } else {
	std::cout << "CHOOSE ONE!!!" << std::endl;
	C[m][0][fm2] = true;
      }
    }
#else
    if (frozen[fi]) {
      C[m][0][fm2] = false;
    } else {
      if (P[m][0][0] > P[m][0][1]) {
	C[m][0][fm2] = false;
      } else {
	C[m][0][fm2] = true;
      }
    }
#endif
    if (fm2 == 1) 
      recursivelyUpdateC(m, fi);
  }
  for (int i = 0; i < N; ++i) {
    c[i] = C[0][i][0];
  }
}

inline double Decoder::W(double y, double x) {
  return 1 / (1 + exp((-1)*x*2 * y / (stddev*stddev)));
}

void Decoder::recursivelyCalcP(int32_t lambda, int32_t fi) {
  if (lambda == 0)
    return;
  int32_t fm2 = fi%2;
  if (fm2 == 0)
    recursivelyCalcP(lambda - 1, fi/2);
  double maximum = 0;
  for (int beta = 0; beta < (1 << (m - lambda)); ++beta) {
    if (fm2 == 0) {
      P[lambda][beta][0]  = P[lambda-1][2*beta][ 0 ] * P[lambda-1][2*beta+1][ 0 ];
      P[lambda][beta][0] += P[lambda-1][2*beta][ 1 ] * P[lambda-1][2*beta+1][ 1 ];
      P[lambda][beta][1]  = P[lambda-1][2*beta][ 1 ] * P[lambda-1][2*beta+1][ 0 ];
      P[lambda][beta][1] += P[lambda-1][2*beta][ 0 ] * P[lambda-1][2*beta+1][ 1 ];
      if (maximum < P[lambda][beta][0]) maximum = P[lambda][beta][0];
      if (maximum < P[lambda][beta][1]) maximum = P[lambda][beta][1];
    } else {
      bool u = (C[lambda][beta][0])? 1 : 0 ;
      P[lambda][beta][0] = P[lambda-1][2*beta][ u ] * P[lambda-1][2*beta+1][ 0 ];
      P[lambda][beta][1] = P[lambda-1][2*beta][u^1] * P[lambda-1][2*beta+1][ 1 ];
      if (maximum < P[lambda][beta][0]) maximum = P[lambda][beta][0];
      if (maximum < P[lambda][beta][1]) maximum = P[lambda][beta][1];
    }
  }
  if (maximum > 0) {
    for (int beta = 0; beta < (1 << (m - lambda)); ++beta) {
      P[lambda][beta][0] /= maximum;
      P[lambda][beta][1] /= maximum;
    }
  }
}

void Decoder::recursivelyUpdateC(int32_t lambda, int32_t fi) {
  assert(fi%2 == 1);
  int32_t psi = fi/2;
  int32_t psm2 = psi%2;
  for (int beta = 0; beta < (1 << (m - lambda)); ++beta) {
    C[lambda-1][ 2*beta ][psm2] = C[lambda][beta][0] != C[lambda][beta][1];
    C[lambda-1][2*beta+1][psm2] = C[lambda][beta][1] ;
  }
  if (psm2 == 1)
    recursivelyUpdateC(lambda-1, psi);
}
