#ifndef _DECODER_H_
#define _DECODER_H_

#include <cmath>
#include <cstring>
#include "common.h"
#include "code.h"

class Decoder {
  const int32_t m;
  const int32_t N;
  const int32_t K;
  const double stddev;
  bool *frozen;
  bool ***C;
  int32_t *indeces;
  int32_t *shuffle;
  double ***P;
  
 public:
  Decoder(int32_t n, int32_t k, int32_t *ind, int32_t *shuf, double sd);
  ~Decoder();
  void decode (double *y, bool *c);

 private:
  inline double W(double y, double x);
  void recursivelyCalcP(int32_t lambda, int32_t fi);
  void recursivelyUpdateC(int32_t lambda, int32_t fi);
};

#endif /* _DECODER_H_ */
