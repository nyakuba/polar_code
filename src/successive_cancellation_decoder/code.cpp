#include "include/code.h"

void input_seq(std::string &input, bool *u, int32_t K) {
  for (int i = 0; i < K; ++i) {
    switch (input[i]) {
    case '0':
      u[i] = false;
      break;
    case '1':
      u[i] = true;
      break;
    default: 
      std::cout << "invalid input" << std::endl;
      return;
    }
  }
}

void gen_random_input(bool *u, int32_t K) {
  std::mt19937 g;
  for (int i = 0; i < K; ++i) {
    // u[i] = false;
    switch ((int)g()&1) {
    case 0: u[i] = true; break;
    case 1: u[i] = false; break;
    }
  }
}
