#ifndef _CHANNEL_H_
#define _CHANNEL_H_

#include <random>
#include "common.h"
#include "code.h"

void sendAWGN(bool *v, double *y, int32_t n, std::default_random_engine &gen, std::normal_distribution<double> &dist);

#endif /* _CHANNEL_H_ */
