#ifndef _MINMAXHEAP_H_
#define _MINMAXHEAP_H_

#include <iostream>
#include <cassert>
#include <cstdint>
#include <cmath>

/* typedef int32_t T; */
template<class T, bool (*_cmp)(const T & a, const T & b)>
class MinMaxHeap {
 private:
    const int32_t _capacity;
    int32_t _size;
    T *_values;

 public:
    MinMaxHeap(int32_t capacity): 
    _capacity(capacity), 
    _size(0)
    {
	_values = new T[_capacity];
    }
    ~MinMaxHeap() {
	delete [] _values;
    }
    inline T min() const {
	assert(_size > 0);
	return *_values;
    }
    inline T max() const {
	assert(_size > 0);
	if (_size == 1) return *_values;
	if (_size == 2) return *(_values+1);
	return (_values[1] > _values[2])? _values[1] : _values[2];
    }
    inline void pop_min() {
	assert(_size > 0);
	_values[0] = _values[_size-1];
	--_size;
	trickle_down_min(1);
    }
    inline void pop_max() {
	assert(_size > 0);
	if (_size < 2) {
	    --_size;
	    return;
	}
	int32_t m = (_values[1] > _values[2])? 1 : 2;
	_values[m] = _values[_size-1];
	--_size;
	trickle_down_max_from_top(m+1);
    }
    void push(const T & value) {
	assert(_size <= _capacity);
	_values[_size++] = value;
	bubble_up(_size);
    }

    inline bool empty() const { return _size == 0; }

    inline int32_t size() const { return _size; }

    inline int32_t capacity() const { return _capacity; }

    inline void clear() { _size = 0; }

    void print_values() {
	if (empty()) {
	    std::cout << "empty" << std::endl;
	} else {
	    for (int32_t i = 0; i < _size; ++i) {
		std::cout << "<" << _values[i] << " " << _values[i] << ">";
	    }
	    std::cout << std::endl;
	}
    }

 private:
    // all subtrees of index must be max(min) ordered, but 
    // whole tree of index can not be max(min) ordered.
    // so used by pop operations.
    void trickle_down(int32_t index) {
	if (current_level_min(index)) {
	    trickle_down_min(index);
	} else {
	    trickle_down_max(index);
	}
    }

    void trickle_down_min(int32_t index) {
	if (has_child(index)) {
	    int32_t m = min_child(index);
	    if (m > rchild(index)) {
		if (_cmp(_values[m-1], _values[index-1])) {
		    swap(m-1, index-1);
		    if (_cmp(_values[parent(m)-1], _values[m-1])) {
			swap(m-1, parent(m)-1);
		    }
		    trickle_down_min(m);
		}
	    } else {
		if (_cmp(_values[m-1], _values[index-1])) {
		    swap(index-1, m-1);
		}
	    }
	}
    }

    void trickle_down_max(int32_t index) {
	if (has_child(index)) {
	    int32_t m = max_child(index);
	    if (m > rchild(index)) {
		if (_cmp(_values[index-1], _values[m-1])) {
		    swap(m-1, index-1);
		    if (_cmp(_values[m-1], _values[parent(m)-1])) {
			swap(m-1, parent(m)-1);
		    }
		    trickle_down_max(m);
		}
	    } else {
		if (_cmp(_values[index-1], _values[m-1])) {
		    swap(index-1, m-1);
		}
	    }
	} 
    }

    void trickle_down_max_from_top(int32_t index) {
	if (has_child(2) && has_child(3)) {
	    int32_t m = max_child(2);
	    int32_t m1 = max_child(3);
	    int32_t i = 2;
	    if (_cmp(_values[m-1], _values[m1-1])) {
		m = m1;
		i = 3;
	    }
	    if (m > rchild(index)) {
		if (_cmp(_values[index-1], _values[m-1])) {
		    swap(m-1, index-1);
		    if (_cmp(_values[m-1], _values[parent(m)-1])) {
			swap(m-1, parent(m)-1);
		    }
		    trickle_down_max(m);
		    if (index != i) {
			trickle_down_max(i);
		    }
		}
	    } else {
		if (_cmp(_values[index-1], _values[m-1])) {
		    swap(index-1, m-1);
		}
	    }
	} else if (has_child(index)) {
	    int32_t m = max_child(index);
	    if (m > rchild(index)) {
		if (_cmp(_values[index-1], _values[m-1])) {
		    swap(m-1, index-1);
		    if (_cmp(_values[m-1], _values[parent(m)-1])) {
			swap(m-1, parent(m)-1);
		    }
		    trickle_down_min(m);
		}
	    } else {
		if (_cmp(_values[index-1], _values[m-1])) {
		    swap(index-1, m-1);
		}
	    }
	} 
    }

    // used by insertion on first available leaf position
    void bubble_up(int32_t index) {
	if (current_level_min(index)) {
	    if (has_parent(index) 
		&& _cmp(_values[parent(index)-1], _values[index-1])) {
		swap(index-1, parent(index)-1);
		bubble_up_max(parent(index));
	    } else {
		bubble_up_min(index);
	    }
	} else {
	    if (has_parent(index) 
		&& _cmp(_values[index-1], _values[parent(index)-1])) {
		swap(index-1, parent(index)-1);
		bubble_up_min(parent(index));
	    } else {
		bubble_up_max(index);
	    }
	}
    }

    void bubble_up_min(int32_t index) {
	if (has_grandparent(index)) {
	    if (_cmp(_values[index-1], _values[grandparent(index)-1])) {
		swap(index-1, grandparent(index)-1);
		bubble_up_min(grandparent(index));
	    } 
	}
    }

    void bubble_up_max(int32_t index) {
	if (has_grandparent(index)) {
	    if (_cmp(_values[grandparent(index)-1], _values[index-1])) {
		swap(index-1, grandparent(index)-1);
		bubble_up_max(grandparent(index));
	    } 
	}	
    }

    bool current_level_min(int32_t index) {
	return (((int32_t)log2(index)) % 2) != 1;
    }

    int32_t min_child(const int32_t & index) const {
	assert(has_child(index));
	int32_t  ch_i = index*2;
	int32_t min = ch_i;
	T *min_val = _values + ch_i - 1;

	// check right child
	if (ch_i+1 <= _size) {
	    if (_cmp(_values[ch_i], *min_val)) {
		++min; ++min_val;
	    }
	} else {
	    return min;
	}
	ch_i *= 2;
	// check left left grandchild
	if (ch_i <= _size) {
	    if (_cmp(_values[ch_i-1], *min_val)) {
		min = ch_i; min_val = _values + ch_i - 1;
	    }
	} else {
	    return min;
	}
	++ch_i;
	// check left right grandchild
	if (ch_i <= _size) {
	    if (_cmp(_values[ch_i-1], *min_val)) {
		min = ch_i; min_val = _values + ch_i - 1;
	    }
	} else {
	    return min;
	}
	++ch_i;
	// check right left grandchild
	if (ch_i <= _size) {
	    if (_cmp(_values[ch_i-1], *min_val)) {
		min = ch_i; min_val = _values + ch_i - 1;
	    }
	} else {
	    return min;
	}
	++ch_i;
	// check right right grandchild
	if (ch_i <= _size && _cmp(_values[ch_i-1], *min_val)) {
		min = ch_i; min_val = _values + ch_i - 1;
	}

	return min;
    }

    int32_t max_child(const int32_t & index) const {
	assert(has_child(index));
	// default left child
	int32_t  ch_i = index*2;
	int32_t max = ch_i;
	T *max_val = _values + ch_i - 1;

	// check right child
	if (ch_i+1 <= _size) {
	    if (_cmp(*max_val, _values[ch_i])) {
		++max; ++max_val;
	    }
	} else {
	    return max;
	}
	ch_i *= 2;
	// check left left grandchild
	if (ch_i <= _size) {
	    if (_cmp(*max_val, _values[ch_i-1])) {
		max = ch_i; max_val = _values + ch_i - 1;
	    }
	} else {
	    return max;
	}
	++ch_i;
	// check left right grandchild
	if (ch_i <= _size) {
	    if (_cmp(*max_val, _values[ch_i-1])) {
		max = ch_i; max_val = _values + ch_i - 1;
	    }
	} else {
	    return max;
	}
	++ch_i;
	// check right left grandchild
	if (ch_i <= _size) {
	    if (_cmp(*max_val, _values[ch_i-1])) {
		max = ch_i; max_val = _values + ch_i - 1;
	    }
	} else {
	    return max;
	}
	++ch_i;
	// check right right grandchild
	if (ch_i <= _size && _cmp(*max_val, _values[ch_i-1])) {
	    max = ch_i; max_val = _values + ch_i - 1;
	}

	return max;
    }

    int32_t  lchild(const int32_t & index) const { return index*2  ; }
    int32_t  rchild(const int32_t & index) const { return index*2+1; }
    int32_t llchild(const int32_t & index) const { return index*4  ; }
    int32_t lrchild(const int32_t & index) const { return index*4+1; }
    int32_t rlchild(const int32_t & index) const { return index*4+2; }
    int32_t rrchild(const int32_t & index) const { return index*4+3; }
    int32_t  parent(const int32_t & index) const { return index/2  ; }
    int32_t grandparent(const int32_t & index) const { return index/4; }
    bool has_child(const int32_t & index) const { return lchild(index) <= _size; }
    bool has_parent(const int32_t & index) const { return index/2 != 0; }
    bool has_grandparent(const int32_t & index) const { return has_parent(index) && parent(index)/2 != 0; }
    void swap(const int32_t & a, const int32_t & b) {
	assert(a >= 0 && b >= 0);
	assert(a < _size && b < _size);
	T tmp = _values[a];
	_values[a] = _values[b];
	_values[b] = tmp;
    }

    /* bool _cmp(const T & a, const T & b) const { return a < b; } */
};

#endif /* _MINMAXHEAP_H_ */
