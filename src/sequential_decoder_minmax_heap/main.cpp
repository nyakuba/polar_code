#include <iostream>
#include <ctime>
#include "include/GaussianApproximation.h"
#include "include/code.h"
#include "include/common.h"
#include "include/encoding.h"
#include "include/channel.h"
#include "include/decoder.h"

int main(int argc, char *argv[]) {

    // initialization
    int32_t test_count = 0, iter_count = 0, not_decoded_max_count = 0, N = 0, K = 0, list_size = 1, Theta = 1;
    int32_t iter_counter = 0;
    double err_prob = 0;
    std::cout << "Test count : ";
    std::cin >> test_count;
    std::cout << test_count << std::endl;
    std::cout << "Max iterations count : ";
    std::cin >> iter_count;
    std::cout << iter_count << std::endl;
    std::cout << "Not decoded max count : ";
    std::cin >> not_decoded_max_count;
    std::cout << not_decoded_max_count << std::endl;
    std::cout << "N, K, err_prob, list_size, Theta" << std::endl;
    std::cin >> N >> K >> err_prob >> list_size >> Theta;
    std::cout << N << " " << K << " " << err_prob << " " << list_size << " " << Theta << std::endl;
    std::string str_u;
    time_t start, end, total_time = 0;
    int32_t not_decoded = 0, not_decoded_iter = 0;
    double snr = 0, snr1 = 0, stddev = 0, stddev1 = 0;
    double *z_init = new double[N];
    double *z = new double[N];
    double *y = new double[N];
    int32_t *indeces = new int32_t[N];
    int32_t *shuffle = new int32_t[N];
    int32_t *tmp = new int32_t[N];
    int32_t *work = new int32_t[N];
    bool *u = new bool[K];
    bool *v = new bool[N];
    bool *work1 = new bool[N];
    bool *c = new bool[N];
    bool *dec = new bool[N];

    memset(indeces, 0, sizeof(int32_t)*N);
    memset(shuffle, 0, sizeof(int32_t)*N);
    memset(tmp, 0, sizeof(int32_t)*N);
    memset(work, 0, sizeof(int32_t)*N);
    memset(z_init, 0, sizeof(double)*N);
    memset(z, 0, sizeof(double)*N);
    memset(y, 0, sizeof(double)*N);
    memset(u, 0, sizeof(bool)*K);
    memset(v, 0, sizeof(bool)*N);
    memset(work1, 0, sizeof(bool)*N);
    memset(c, 0, sizeof(bool)*N);
    memset(dec, 0, sizeof(bool)*N);
    int32_t *pi = indeces, *ps = shuffle;
    for (int i = 0; i < N; ++i) {
	*pi = i; *ps = i;
	++pi; ++ps;
    }

    std::cout << "Code SNR: ";
    std::cin >> snr;
    std::cout << snr << std::endl;
    stddev = sqrt(1/(2 * pow(10, (snr/10)) * ((double)K/N)));
    std::cout << "Standard deviation: " << stddev << std::endl;
    std::cout << "Channel SNR: ";
    std::cin >> snr1;
    std::cout << snr1 << std::endl;
    stddev1 = sqrt(1/(2 * pow(10, (snr1/10)) * ((double)K/N)));
    std::cout << "Standard deviation: " << stddev1 << std::endl;

    //1. **************************************************//
#ifndef NO_LOGS
    std::cout << "Constructing polar code (" << N << ", " << K << ") for BEC with erasure probability = " << err_prob << "." << std::endl;
    std::cout << "Calculating Bhattacharyya parameters for channel." << std::endl;
#endif

    start = clock();
    GetSubchannelQuality(log2(N), stddev, z);
    end = clock();
    total_time += end - start;
    memcpy(z_init, z, sizeof(double)*N);
    // std::cout << "z : ";
    // println_seq(z, N);

#ifndef NO_LOGS
    std::cout << "Time consumed: " << end - start << std::endl;
    std::cout << "Total time: " << total_time << std::endl;
    std::cout << "Sorting parameters." << std::endl;
#endif

    start = clock();
    sortZ(z, indeces, N, 0, N);
    end = clock();
    total_time += end - start;

#ifndef NO_LOGS
    std::cout << "Time consumed: " << end - start << std::endl;
    std::cout << "Total time: " << total_time << std::endl;
    std::cout << "Forming reverse shuffle permutation array." << std::endl;
#endif

    start = clock();
    reverse_shuffle(shuffle, N, tmp, work);
    end = clock();
    total_time += end - start;

#ifndef NO_LOGS
    std::cout << "Time consumed: " << end - start << std::endl;
    std::cout << "Total time: " << total_time << std::endl;  
    std::cout << "Code initialization completed." << std::endl;
    std::cout << "Time consumed: " << total_time << std::endl;
#endif

    // std::cout << "z : ";
    // println_seq(z, N);
    // std::cout << "indeces : ";
    // println_seq(indeces, N);
    // std::cout << "shuffle : ";
    // println_seq(shuffle, N);

    //2. **************************************************//
#ifndef NO_LOGS
    std::cout << "Word to encode: ";
#endif
    std::cin >> str_u;
#ifndef NO_LOGS
    std::cout << str_u << std::endl;
#endif
    std::default_random_engine gen;
    std::normal_distribution<double> dist(0.0, stddev1);
  
    // for test_count loop
    for (int test_counter = 0; test_counter < test_count; ++test_counter) {
	if (str_u.compare("random") == 0) {
	    gen_random_input(u, K);
	} else {
	    input_seq(str_u, u, K);
	}
#ifndef NO_LOGS
	std::cout << "Encoding." << std::endl;
#endif

	start = clock();
	for (int i = 0; i < K; ++i) {
	    work1[indeces[i]] = u[i];
	}
	// println_seq(indeces, N);
	// println_seq(work1, N);
	encode(work1, v, N, shuffle);
	end = clock();
	total_time += end - start;

#ifndef NO_LOGS
	std::cout << "Time consumed: " << end - start << std::endl;
	std::cout << "Total time: " << total_time << std::endl;  
#endif

	SequentialDecoder decoder(N, K, list_size, Theta, stddev, indeces, z_init);

	while (iter_counter < iter_count && not_decoded < not_decoded_max_count) {
#ifndef NO_LOGS
	    std::cout << "Sending encoded word over AWGN channel." << std::endl;
#endif
	    start = clock();
	    sendAWGN(v, y, N, gen, dist);
	    end = clock();
	    total_time += end - start;
#ifndef NO_LOGS
	    std::cout << "Time consumed: " << end - start << std::endl;
	    std::cout << "Total time: " << total_time << std::endl;
#endif

	    // std::cout << "u : ";
	    // println_seq(u, K, true);
	    // std::cout << "v : ";
	    // println_seq(v, N, true);
	    // std::cout << "y : ";
	    // println_seq(y, N);

	    //3. **************************************************//
#ifndef NO_LOGS
	    std::cout << "Decoding." << std::endl;
#endif

	    start = clock();
	    decoder.decode(y, c);
	    encode(c, dec, N, shuffle);
	    end = clock();
	    total_time += end - start;

#ifndef NO_LOGS
	    std::cout << "Time consumed: " << end - start << std::endl;
	    std::cout << "Total time: " << total_time << std::endl;  
#endif

	    // std::cout << "codeword \t";
	    // println_seq(work1, N, true);
	    // std::cout << "decoded word \t";
	    // println_seq(dec, N, true);
	    int32_t error_count = 0;
	    for (int i = 0; i < N; ++i) {
		if (work1[indeces[i]] != dec[indeces[i]]) {
		    ++error_count;
		}
	    }

#ifndef NO_LOGS
	    std::cout << "Error count : " << error_count << std::endl;
	    std::cout << "Number of cmp ops: " << decoder.get_cmp_ops_count() << std::endl;
	    std::cout << "Number of add ops: " << decoder.get_add_ops_count() << std::endl;
#endif
	    if (error_count > 0) {
		std::cout << "+ " << not_decoded_iter << std::endl;
		++not_decoded_iter;
		++not_decoded;
	    }
	    ++iter_counter;
	}
	std::cout << "Not decoded test cases on test " << test_counter << ": " << not_decoded_iter << std::endl;
	std::cout << "Iterations : " << iter_counter << std::endl;
	std::cout << "Error probability : " << (double)not_decoded_iter/iter_counter << std::endl;
	std::cout << "Total number of cmp ops: " << decoder.get_cmp_ops_count() << std::endl;
	std::cout << "Total number of add ops: " << decoder.get_add_ops_count() << std::endl;

	iter_counter = 0;
	not_decoded_iter = 0;
    } // test_count
    std::cout << "Not decoded test cases count : " << not_decoded << std::endl;
    std::cout << "Total time consumed: " << total_time << std::endl;

    delete [] z;
    delete [] y;
    delete [] indeces;
    delete [] shuffle;
    delete [] tmp;
    delete [] work;
    delete [] u;
    delete [] v;
    delete [] work1;
    delete [] c;
    delete [] dec;

    return 0;
}
