#include "include/decoder.h"

// #define LOG_FLOW
// #define LOG_CHOOSE

#include <fstream>

int64_t SequentialDecoder::cmp_ops_count = 0;
int64_t SequentialDecoder::add_ops_count = 0;

void SequentialDecoder::decode(double *y, bool *c) {
    clean_data_structures();

    int32_t l = assignInitialPath();
    fi[l] = 0;
    *(getPointer_D(0, l)) = 0;
    double *Sl = getArrayPointer_S(0, l);
    for (int32_t beta = 0; beta < n; ++beta) {
	Sl[beta] = initS(y[beta]);
    }

    paths.push(PAIR(0, l));

    int32_t min_fi = 0;
    while (!paths.empty()) {
	const PAIR p = paths.min();
	paths.pop_min();
	l = p.second;
	if (fi[l] < min_fi) {
	    killPath(l);
	    continue;
	}
	++q[fi[l]];
	if (fi[l] == n) {
	    const bool **Cl = readArrayPointer_C(0, l);
	    for (int32_t beta = 0; beta < n; ++beta) {
		c[beta] = Cl[beta][0];
	    }
	    return;
	}
	recursivelyCalcS(l, m, fi[l]);
	if (frozen[fi[l]]) { 
#ifdef LOG_CHOOSE
	    std::cout << "###FROZEN BIT" << std::endl;
#endif
	    bool **Clm = getArrayPointer_C(m, l);
	    const double *Slm = readArrayPointer_S(m, l);
	    const double *Dlm = readPointer_D(m, l);
	    Clm[0][fi[l] % 2] = false;

	    paths.push(PAIR(plus(plus(Slm[0], *Dlm), O[fi[l]]), l));

	    if (fi[l] % 2 == 1) {
		recursivelyUpdateC(l, m, fi[l]);
	    }
	    ++fi[l];
	} else { 
#ifdef LOG_CHOOSE
	    std::cout << "###UNFROZEN BIT" << std::endl;
#endif
	    // unfrozen bit
	    // Theta must be > 1, much greater than L
	    while (paths.size() >= Theta - 1) { 
		PAIR min = paths.max();
	    	killPath(min.second);
		paths.pop_max();
	    }

	    const double *Slm = readArrayPointer_S(m, l);
	    const double *Dlm = readPointer_D(m, l);

	    bool **Clm = getArrayPointer_C(m, l);
	    Clm[0][fi[l] % 2] = false;

	    int32_t l1 = clonePath(l);

	    bool **Cl1m = getArrayPointer_C(m, l1);
	    Cl1m[0][fi[l] % 2] = true;
	    paths.push(PAIR(plus(plus(Slm[0], *Dlm), O[fi[l]]), l));
	    paths.push(PAIR(plus(*Dlm, O[fi[l]]), l1));

	    if (fi[l] % 2 == 1) {
		recursivelyUpdateC(l, m, fi[l]);
		recursivelyUpdateC(l1, m, fi[l]);
	    }
	    fi[l1] = ++fi[l];

	    if (q[fi[l]] >= L) {
		min_fi = fi[l];
	    }
	}
    }
}

void SequentialDecoder::recursivelyCalcS(int32_t l, int32_t lambda, int32_t fi) {
#ifdef LOG_FLOW
    std::cout << "start recursivelyCalcS" << std::endl;
#endif
    if (lambda == 0) return;
    if (fi % 2 == 0) recursivelyCalcS(l, lambda-1, fi/2);
    int32_t size = (1 << (m-lambda));
    double *Dll = getPointer_D(lambda, l);
    double *Sll = getArrayPointer_S(lambda, l);
    const double *Dll_1 = readPointer_D(lambda-1, l);
    const double *Sll_1 = readArrayPointer_S(lambda-1, l);
    *Dll = *Dll_1;
    if (fi % 2 == 0) {
	for (int32_t beta = 0; beta < size; ++beta) {
	    Sll[beta] = Q(Sll_1[2*beta], Sll_1[2*beta+1]);
	    add(*Dll, std::max(Sll_1[2*beta], Sll_1[2*beta+1]));
	}
    } else {
	const bool **Cll = readArrayPointer_C(lambda, l);
	for (int32_t beta = 0; beta < size; ++beta) {
	    if (Cll[beta][0] == false) {
		Sll[beta] = plus(Sll_1[2*beta+1],  Sll_1[2*beta]);
	    } else {
		Sll[beta] = plus(Sll_1[2*beta+1], -Sll_1[2*beta]);
		add(*Dll, Sll_1[2*beta]);
	    }
	}
    }
#ifdef LOG_FLOW
    std::cout << "end recursivelyCalcS" << std::endl;
#endif
}

void SequentialDecoder::recursivelyUpdateC(int32_t l, int32_t lambda, int32_t fi) {
#ifdef LOG_FLOW
    std::cout << "start recursivelyUpdateC" << std::endl;
#endif
    assert(fi % 2 == 1);
    int32_t psi = fi/2;
    int32_t psim2 = psi % 2;
    int32_t size = 1 << (m - lambda);
    bool **Cl_1 = getArrayPointer_C(lambda - 1, l);
    const bool **Cl = readArrayPointer_C(lambda, l);
    for (int32_t beta = 0; beta < size; ++beta) {
	Cl_1[ 2*beta ][psim2] = bxor(Cl[beta][0], Cl[beta][1]);
	Cl_1[2*beta+1][psim2] = Cl[beta][1];
    }
    if (psim2 == 1)
	recursivelyUpdateC(l, lambda - 1, psi);
#ifdef LOG_FLOW
    std::cout << "end recursivelyUpdateC" << std::endl;
#endif
}

int32_t SequentialDecoder::assignInitialPath() {
#ifdef LOG_FLOW
    std::cout << "start assignInitialPath" << std::endl;
#endif
    int32_t l = inactivePathIndeces.top();
    inactivePathIndeces.pop();
    // Associate arrays with path index
    int32_t s = 0;
    for (int32_t lambda = 0; lambda <= m; ++lambda) {
	s = inactiveArrayIndeces[lambda].top();
	inactiveArrayIndeces[lambda].pop();
	pathIndexToArrayIndex[lambda][l] = s;
	arrayReferenceCount[lambda][s] = 1;
    }
#ifdef LOG_FLOW
    std::cout << "end assignInitialPath" << std::endl;
#endif
    return l;
}

int32_t SequentialDecoder::clonePath(int32_t l) {
#ifdef LOG_FLOW
    std::cout << "start clonePath" << std::endl;
#endif
    int32_t l1 = inactivePathIndeces.top();
    inactivePathIndeces.pop();
    // Make l1 reference same arrays as l
    int32_t s = 0;
    for (int32_t lambda = 0; lambda <= m; ++lambda) {
	s = pathIndexToArrayIndex[lambda][l];
	pathIndexToArrayIndex[lambda][l1] = s;
	++arrayReferenceCount[lambda][s];
    }
#ifdef LOG_FLOW
    std::cout << "end clonePath" << std::endl;
#endif
    return l1;
}

void SequentialDecoder::killPath(int32_t l) {
#ifdef LOG_FLOW
    std::cout << "start KillPath " << l << std::endl;
#endif
    //Mark the path index l as inactive
    inactivePathIndeces.push(l);
    //Disassociate array with path index
    int32_t s = 0;
    for (int32_t lambda = 0; lambda <= m; ++lambda) {
	s = pathIndexToArrayIndex[lambda][l];
	--arrayReferenceCount[lambda][s];
	if (arrayReferenceCount[lambda][s] == 0) 
	    inactiveArrayIndeces[lambda].push(s);
    }
#ifdef LOG_FLOW
    std::cout << "end KillPath " << l << std::endl;
#endif
}

const double * SequentialDecoder::readPointer_D(int32_t lambda, int32_t l) {
#ifdef LOG_FLOW
    std::cout << "readPointer_D " << lambda << " " << l << std::endl;
#endif
    return const_cast<const double*>(D[lambda] + pathIndexToArrayIndex[lambda][l]);
}

const double * SequentialDecoder::readArrayPointer_S(int32_t lambda, int32_t l) {
#ifdef LOG_FLOW
    std::cout << "readArrayPointer_S " << lambda << " " << pathIndexToArrayIndex[lambda][l] << std::endl;
#endif
    return const_cast<const double*>(arrayPointer_S[lambda][pathIndexToArrayIndex[lambda][l]]);
}

const bool ** SequentialDecoder::readArrayPointer_C(int32_t lambda, int32_t l) {
#ifdef LOG_FLOW
    std::cout << "readArrayPointer_C " << lambda << " " << pathIndexToArrayIndex[lambda][l] << std::endl;
#endif
    return const_cast<const bool**>(arrayPointer_C[lambda][pathIndexToArrayIndex[lambda][l]]);
}

double * SequentialDecoder::getPointer_D(int32_t lambda, int32_t l) {
#ifdef LOG_FLOW
    std::cout << "getPointer_D " << lambda << " " << l << std::endl;
#endif
    int32_t s = pathIndexToArrayIndex[lambda][l];
    int32_t s1 = 0;
    if (arrayReferenceCount[lambda][s] == 1) {
	s1 = s;
    } else {
	s1 = inactiveArrayIndeces[lambda].top();
	inactiveArrayIndeces[lambda].pop();
	// copy all
	memcpy(arrayPointer_S[lambda][s1], arrayPointer_S[lambda][s], sizeof(double)*(1 << (m-lambda)));
	D[lambda][s1] = D[lambda][s];
	for (int32_t i = 0; i < (1 << (m - lambda)); ++i) {
	    memcpy(arrayPointer_C[lambda][s1][i], arrayPointer_C[lambda][s][i], sizeof(bool)*2);
	}
	--arrayReferenceCount[lambda][s];
	arrayReferenceCount[lambda][s1] = 1;
	pathIndexToArrayIndex[lambda][l] = s1;
    }
    return D[lambda] + s1;
}

double * SequentialDecoder::getArrayPointer_S(int32_t lambda, int32_t l) {
#ifdef LOG_FLOW
    std::cout << "getArrayPointer_S " << lambda << " " << pathIndexToArrayIndex[lambda][l] << std::endl;
#endif
    int32_t s = pathIndexToArrayIndex[lambda][l];
    int32_t s1 = 0;
    if (arrayReferenceCount[lambda][s] == 1) {
	s1 = s;
    } else {
	s1 = inactiveArrayIndeces[lambda].top();
	inactiveArrayIndeces[lambda].pop();
	// copy all
	memcpy(arrayPointer_S[lambda][s1], arrayPointer_S[lambda][s], sizeof(double)*(1 << (m-lambda)));
	D[lambda][s1] = D[lambda][s];
	for (int32_t i = 0; i < (1 << (m - lambda)); ++i) {
	    memcpy(arrayPointer_C[lambda][s1][i], arrayPointer_C[lambda][s][i], sizeof(bool)*2);
	}
	--arrayReferenceCount[lambda][s];
	arrayReferenceCount[lambda][s1] = 1;
	pathIndexToArrayIndex[lambda][l] = s1;
    }
    return arrayPointer_S[lambda][s1];
}

bool ** SequentialDecoder::getArrayPointer_C(int32_t lambda, int32_t l) {
#ifdef LOG_FLOW
    std::cout << "getArrayPointer_C " << lambda << " " << pathIndexToArrayIndex[lambda][l] << std::endl;
#endif
    int32_t s = pathIndexToArrayIndex[lambda][l];
    int32_t s1 = 0;
    if (arrayReferenceCount[lambda][s] == 1) {
	s1 = s;
    } else {
	s1 = inactiveArrayIndeces[lambda].top();
	inactiveArrayIndeces[lambda].pop();
	// copy all
	memcpy(arrayPointer_S[lambda][s1], arrayPointer_S[lambda][s], sizeof(double)*(1 << (m-lambda)));
	D[lambda][s1] = D[lambda][s];
	for (int32_t i = 0; i < (1 << (m - lambda)); ++i) {
	    memcpy(arrayPointer_C[lambda][s1][i], arrayPointer_C[lambda][s][i], sizeof(bool)*2);
	}
	--arrayReferenceCount[lambda][s];
	arrayReferenceCount[lambda][s1] = 1;
	pathIndexToArrayIndex[lambda][l] = s1;
    }
    return arrayPointer_C[lambda][s1];
}

SequentialDecoder::SequentialDecoder
(
 int32_t n, 
 int32_t k, 
 int32_t L, 
 int32_t theta,
 double stddev,
 int32_t *indeces, 
 double *z
 ):
    n(n), 
    k(k), 
    m(log2(n)), 
    L(L), 
    Theta(theta),
    stddev(stddev),
    inactivePathIndeces(Theta), 
    inactiveArrayIndeces(m+1, Stack<int32_t>(Theta)),
    paths(theta)
{
    // frozen array
    frozen = new bool[n];
    memset(frozen, 0, sizeof(bool)*n);
    for (int i = n-1; i >= k; --i) {
	frozen[indeces[i]] = true;
    }

    // Omega, fi, q,
    fi = new int32_t[Theta];
    q = new int32_t[n];
    O = new double[n];
    fillOmega(indeces, z, frozen);

    // inactiveArrayIndeces, arrayPointer_C, arrayPointer_S, 
    // arrayReferenceCount, D, pathIndexToArrayIndex
    D = new double*[m+1];
    arrayPointer_S = new double**[m+1];
    arrayPointer_C = new bool***[m+1];
    arrayReferenceCount = new int32_t*[m+1];
    pathIndexToArrayIndex = new int32_t*[m+1];
    for (int32_t lambda = 0; lambda <= m; ++lambda) {
	arrayPointer_S[lambda] = new double*[Theta];
	arrayPointer_C[lambda] = new bool**[Theta];
	for (int32_t l = 0; l < Theta; ++l) {
	    int32_t size = 1 << (m-lambda);
	    arrayPointer_S[lambda][l] = new double[size];
	    arrayPointer_C[lambda][l] = new bool*[size];
	    for (int32_t beta = 0; beta < size; ++beta) {
		arrayPointer_C[lambda][l][beta] = new bool[2];
	    }
	    inactiveArrayIndeces[lambda].push(l);
	}
	D[lambda] = new double[Theta];
	arrayReferenceCount[lambda] = new int32_t[Theta];
	pathIndexToArrayIndex[lambda] = new int32_t[Theta];
    }
    // inactivePathIndeces
    for (int32_t l = 0; l < Theta; ++l)
	inactivePathIndeces.push(l);
}

SequentialDecoder::~SequentialDecoder() {
    delete [] frozen;
    delete [] fi;
    delete [] q;
    delete [] O;
    for (int32_t lambda = 0; lambda <= m; ++lambda) {
	for (int l = 0; l < Theta; ++l) {
	    int32_t size = 1 << (m-lambda);
	    for (int beta = 0; beta < size; ++beta) {
		delete [] arrayPointer_C[lambda][l][beta];
	    }
	    delete [] arrayPointer_S[lambda][l];
	    delete [] arrayPointer_C[lambda][l];
	}
	delete [] D[lambda];
	delete [] arrayPointer_S[lambda];
	delete [] arrayPointer_C[lambda];
	delete [] arrayReferenceCount[lambda];
	delete [] pathIndexToArrayIndex[lambda];
    }
    delete [] D;
    delete [] arrayPointer_S;
    delete [] arrayPointer_C;
    delete [] arrayReferenceCount;
    delete [] pathIndexToArrayIndex;
}

void SequentialDecoder::clean_data_structures() {
    paths.clear();
    memset(fi, 0, sizeof(int32_t)*Theta);
    memset(q, 0, sizeof(int32_t)*n);
    for (int32_t lambda = 0; lambda <= m; ++lambda) {
	inactiveArrayIndeces[lambda].clear();
	for (int32_t l = 0; l < Theta; ++l) {
	    inactiveArrayIndeces[lambda].push(l);
	}
	memset(arrayReferenceCount[lambda], 0, sizeof(int32_t)*Theta);
	memset(pathIndexToArrayIndex[lambda], 0, sizeof(int32_t)*Theta);
    }
    inactivePathIndeces.clear();
    for (int32_t l = 0; l < Theta; ++l)
	inactivePathIndeces.push(l);
}

void SequentialDecoder::fillOmega(const int32_t *indeces, const double *z, const bool *frozen) {
    O[n-1] = 0;
    for (int32_t fi = n-2; fi >= 0; --fi) {
	if (frozen[fi+1]) {
	    O[fi] = O[fi+1] + log(1 - z[fi+1]);
	} else {
	    O[fi] = O[fi+1];
	}
    }
}

bool compare(const PAIR & a, const PAIR & b) {
    if (a.first == b.first) 
	return a.second > b.second;
    return a.first > b.first;
}
