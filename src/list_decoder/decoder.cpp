#include "include/decoder.h"
// #define LOG_CHOOSE
// #define LOG_FLOW

void ListDecoder::decode(double *y, bool *c) {
#ifdef LOG_FLOW
  std::cout << "start decode" << std::endl;
#endif
  int32_t l = assignInitialPath();
  double **P0 = getArrayPointer_P(0, l);
  for (int32_t beta = 0; beta < n; ++beta) {
    P0[beta][0] = W(y[beta], -1);
    P0[beta][1] = W(y[beta],  1);
  }

  for (int32_t fi = 0; fi < n; ++fi) {
    recursivelyCalcP(m, fi);

    if (frozen[fi]) {
      continuePath_FrozenBit(fi);
    } else {
      continuePath_UnfrozenBit(fi);
    }
    if (fi % 2 == 1) {
      recursivelyUpdateC(m, fi);
    }
  }
  // Return the best codeword in the list
  l = findMostProbablePath();
  const bool **C0 = readArrayPointer_C(0, l);
  for (int32_t beta = 0; beta < n; ++beta) {
    c[beta] = C0[beta][0];
  }
#ifdef LOG_FLOW
  std::cout << "end decode" << std::endl;
#endif
}

void ListDecoder::continuePath_FrozenBit(int32_t fi) {
#ifdef LOG_FLOW
  std::cout << "start continuePathFrozenBit" << std::endl;
#endif
#ifdef LOG_CHOOSE
  std::cout << "> FROZEN BIT" << std::endl;
#endif
  for (int32_t l = 0; l < L; ++l) {
    if (pathIndexInactive(l)) continue;
    bool **Cm = getArrayPointer_C(m, l);
    Cm[0][fi % 2] = 0; // frozen value
  }
#ifdef LOG_FLOW
  std::cout << "end continuePathFrozenBit" << std::endl;
#endif
}

void ListDecoder::continuePath_UnfrozenBit(int32_t fi) {
#ifdef LOG_FLOW
  std::cout << "start continuePathUnfrozenBit" << std::endl;
#endif
  int32_t fim2 = fi % 2;
  double **probForks = new double*[L];
  bool **contForks = new bool*[L];
  for (int32_t i = 0; i < L; ++i) {
    probForks[i] = new double[2];
    contForks[i] = new bool[2];
    memset(probForks[i], 0, sizeof(double)*2);
    memset(contForks[i], 0, sizeof(bool)*2);
  }
    
  int32_t i = populateProbForks(probForks);  
  int32_t ro = std::min(2*i, L);
  populateContForks(contForks, probForks, ro);
  // kill all non-continuing paths
  for (int32_t l = 0; l < L; ++l) {
    if (pathIndexInactive(l)) continue;
    if (contForks[l][0] == false && contForks[l][1] == false) {
      killPath(l);
    }
  }
  // continue relevant paths and duplicate it when necessary
  for (int32_t l = 0; l < L; ++l) {
    // both forks are bad or invalid
    if (contForks[l][0] == false && contForks[l][1] == false) {
      continue;
    }
    bool **Cm = getArrayPointer_C(m, l);
    if (contForks[l][0] == true && contForks[l][1] == true) {
      // both forks are good
      #ifdef LOG_CHOOSE
      std::cout << "> CLONE" << std::endl;
      #endif
      Cm[0][fim2] = false;
      int32_t l1 = clonePath(l);
      Cm = getArrayPointer_C(m, l1);
      Cm[0][fim2] = true;
    } else {
      // exactly one fork is good
      if (contForks[l][0] == true) {
        #ifdef LOG_CHOOSE
	std::cout << "> CHOOSE 0" << std::endl;
        #endif
	Cm[0][fim2] = false;
      } else {
        #ifdef LOG_CHOOSE
	std::cout << "> CHOOSE 1" << std::endl;
        #endif
	Cm[0][fim2] = true;
      }
    }
  }

  for (int32_t i = 0; i < L; ++i) {
    delete [] probForks[i];
    delete [] contForks[i];
  }
  delete [] probForks;
  delete [] contForks;
#ifdef LOG_FLOW
  std::cout << "end continuePathUnfrozenBit" << std::endl;
#endif
}

int32_t ListDecoder::findMostProbablePath() {
#ifdef LOG_FLOW
  std::cout << "start findMostProbablePath" << std::endl;
#endif
  int32_t l1 = 0;
  double p1 = 0;
  for (int32_t l = 0; l < L; ++l) {
    if (pathIndexInactive(l)) continue;
    const bool **Cm = readArrayPointer_C(m, l);
    const double **Pm = readArrayPointer_P(m, l);
    if (p1 < Pm[0][Cm[0][1]]) {
      l1 = l;
      p1 = Pm[0][Cm[0][1]];
    }
  }
#ifdef LOG_FLOW
  std::cout << "end findMostProbablePath" << std::endl;
#endif
  return l1;
}

void ListDecoder::recursivelyCalcP(int32_t lambda, int32_t fi) {
#ifdef LOG_FLOW
  std::cout << "start recursivelyCalcP" << std::endl;
#endif
  if (lambda == 0) {
#ifdef LOG_FLOW
    std::cout << "end recursivelyCalcP" << std::endl;
#endif
    return;
  }
  int32_t psi = fi/2, fim2 = fi % 2;
  if (fim2 == 0)
    recursivelyCalcP(lambda - 1, psi);
  // Perform the calculation
  int32_t size = 1 << (m - lambda);
  int32_t maximum = 0;
  for (int32_t l = 0; l < L; ++l) {
    if (pathIndexInactive(l)) continue;
    double **Pl = getArrayPointer_P(lambda, l);
    const double **Pl_1 = readArrayPointer_P(lambda-1, l);
    const bool **Cl = readArrayPointer_C(lambda, l);
    for (int32_t beta = 0; beta < size; ++beta) {
      if (fim2 == 0) {
	Pl[beta][0]  = Pl_1[2*beta][ 0 ] * Pl_1[2*beta+1][ 0 ];
	Pl[beta][0] += Pl_1[2*beta][ 1 ] * Pl_1[2*beta+1][ 1 ];
	Pl[beta][1]  = Pl_1[2*beta][ 1 ] * Pl_1[2*beta+1][ 0 ];
	Pl[beta][1] += Pl_1[2*beta][ 0 ] * Pl_1[2*beta+1][ 1 ];
	if (maximum < Pl[beta][0]) maximum = Pl[beta][0];
	if (maximum < Pl[beta][1]) maximum = Pl[beta][1];
      } else {
	bool u = (Cl[beta][0])? 1 : 0 ;
	Pl[beta][0] = Pl_1[2*beta][ u ] * Pl_1[2*beta+1][ 0 ];
	Pl[beta][1] = Pl_1[2*beta][u^1] * Pl_1[2*beta+1][ 1 ];
	if (maximum < Pl[beta][0]) maximum = Pl[beta][0];
	if (maximum < Pl[beta][1]) maximum = Pl[beta][1];
      }
    }
  }
  if (maximum > 0) {
    for (int32_t l = 0; l < L; ++l) {
      if (pathIndexInactive(l)) continue;
      double **Pl = getArrayPointer_P(lambda, l);
      for (int32_t beta = 0; beta < size; ++beta) {
	Pl[beta][0] /= maximum;
	Pl[beta][1] /= maximum;
      }
    }
  }
#ifdef LOG_FLOW
  std::cout << "end recursivelyCalcP" << std::endl;
#endif
}

void ListDecoder::recursivelyUpdateC(int32_t lambda, int32_t fi) {
#ifdef LOG_FLOW
  std::cout << "start recursivelyUpdateC" << std::endl;
#endif
  assert(fi % 2 == 1);
  int32_t psi = fi/2;
  int32_t psim2 = psi % 2;
  int32_t size = 1 << (m - lambda);
  for (int32_t l = 0; l < L; ++l) {
    if (pathIndexInactive(l)) continue;
    const bool **Cl = readArrayPointer_C(lambda, l);
    bool **Cl_1 = getArrayPointer_C(lambda - 1, l);
    for (int32_t beta = 0; beta < size; ++beta) {
      Cl_1[ 2*beta ][psim2] = Cl[beta][0] != Cl[beta][1];
      Cl_1[2*beta+1][psim2] = Cl[beta][1];
    }
  }
  if (psim2 == 1)
    recursivelyUpdateC(lambda - 1, psi);
#ifdef LOG_FLOW
  std::cout << "end recursivelyUpdateC" << std::endl;
#endif
}

ListDecoder::ListDecoder(int32_t n, int32_t k, int32_t L, int32_t *indeces, double stddev):
  n(n), k(k), m(log2(n)), L(L), indeces(indeces), stddev(stddev), inactivePathIndeces(L), inactiveArrayIndeces(m+1, Stack<int32_t>(L))
{
  // frozen array
  frozen = new bool[n];
  memset(frozen, 0, sizeof(bool)*n);
  for (int i = n-1; i >= k; --i) {
    frozen[indeces[i]] = true;
  }

  // activePath
  activePath = new bool[L];
  memset(activePath, 0, sizeof(bool)*L);
  // inactiveArrayIndeces, arrayPointer_P, arrayPointer_C, arrayReferenceCount
  arrayPointer_P = new double***[m+1];
  arrayPointer_C = new bool***[m+1];
  arrayReferenceCount = new int32_t*[m+1];
  pathIndexToArrayIndex = new int32_t*[m+1];
  for (int32_t lambda = 0; lambda <= m; ++lambda) {
    arrayPointer_P[lambda] = new double**[L];
    arrayPointer_C[lambda] = new bool**[L];
    for (int32_t s = 0; s < L; ++s) {
      int32_t size = 1 << (m-lambda);
      arrayPointer_P[lambda][s] = new double*[size];
      arrayPointer_C[lambda][s] = new bool*[size];
      for (int32_t i = 0; i < size; ++i) {
	arrayPointer_P[lambda][s][i] = new double[2];
	arrayPointer_C[lambda][s][i] = new bool[2];
	memset(arrayPointer_P[lambda][s][i], 0, sizeof(double)*2);
	memset(arrayPointer_C[lambda][s][i], 0, sizeof(bool)*2);
      }
      inactiveArrayIndeces[lambda].push(s);
    }
    arrayReferenceCount[lambda] = new int32_t[L];
    pathIndexToArrayIndex[lambda] = new int32_t[L];
    memset(arrayReferenceCount[lambda], 0, sizeof(int32_t)*L);
  }
  // inactivePathIndeces
  for (int32_t l = 0; l < L; ++l)
    inactivePathIndeces.push(l);
}

ListDecoder::~ListDecoder() {
  delete [] activePath;
  delete [] frozen;
  for (int32_t lambda = 0; lambda <= m; ++lambda) {
    for (int s = 0; s < L; ++s) {
      int32_t size = 1 << (m-lambda);
      for (int i = 0; i < size; ++i) {
  	delete [] arrayPointer_P[lambda][s][i];
  	delete [] arrayPointer_C[lambda][s][i];
      }
      delete [] arrayPointer_P[lambda][s];
      delete [] arrayPointer_C[lambda][s];
    }
    delete [] arrayPointer_P[lambda];
    delete [] arrayPointer_C[lambda];
    delete [] arrayReferenceCount[lambda];
    delete [] pathIndexToArrayIndex[lambda];
  }
  delete [] arrayPointer_P;
  delete [] arrayPointer_C;
  delete [] arrayReferenceCount;
  delete [] pathIndexToArrayIndex;
}

int32_t ListDecoder::assignInitialPath() {
#ifdef LOG_FLOW
  std::cout << "start assignInitialPath" << std::endl;
#endif
  int32_t l = inactivePathIndeces.top();
  inactivePathIndeces.pop();
  activePath[l] = true;
  // Associate arrays with path index
  int32_t s = 0;
  for (int32_t lambda = 0; lambda <= m; ++lambda) {
    s = inactiveArrayIndeces[lambda].top();
    inactiveArrayIndeces[lambda].pop();
    pathIndexToArrayIndex[lambda][l] = s;
    arrayReferenceCount[lambda][s] = 1;
  }
#ifdef LOG_FLOW
  std::cout << "end assignInitialPath" << std::endl;
#endif
  return l;
}

int32_t ListDecoder::clonePath(int32_t l) {
#ifdef LOG_FLOW
  std::cout << "start clonePath" << std::endl;
#endif
  int32_t l1 = inactivePathIndeces.top();
  inactivePathIndeces.pop();
  activePath[l1] = true;
  // Make l1 reference same arrays as l
  int32_t s = 0;
  for (int32_t lambda = 0; lambda <= m; ++lambda) {
    s = pathIndexToArrayIndex[lambda][l];
    pathIndexToArrayIndex[lambda][l1] = s;
    ++arrayReferenceCount[lambda][s];
  }
#ifdef LOG_FLOW
  std::cout << "end clonePath" << std::endl;
#endif
  return l1;
}

void ListDecoder::killPath(int32_t l) {
#ifdef LOG_FLOW
  std::cout << "start KillPath " << l << std::endl;
#endif
  //Mark the path index l as inactive
  activePath[l] = false;
  inactivePathIndeces.push(l);
  //Disassociate array with path index
  int32_t s = 0;
  for (int32_t lambda = 0; lambda <= m; ++lambda) {
    s = pathIndexToArrayIndex[lambda][l];
    --arrayReferenceCount[lambda][s];
    if (arrayReferenceCount[lambda][s] == 0) 
      inactiveArrayIndeces[lambda].push(s);
  }
#ifdef LOG_FLOW
  std::cout << "end KillPath " << l << std::endl;
#endif
}

const double ** ListDecoder::readArrayPointer_P(int32_t lambda, int32_t l) {
#ifdef LOG_FLOW
  std::cout << "readArrayPointer_P " << lambda << " " << pathIndexToArrayIndex[lambda][l] << std::endl;
#endif
  return const_cast<const double**>(arrayPointer_P[lambda][pathIndexToArrayIndex[lambda][l]]);
}

const bool ** ListDecoder::readArrayPointer_C(int32_t lambda, int32_t l) {
#ifdef LOG_FLOW
  std::cout << "readArrayPointer_C " << lambda << " " << pathIndexToArrayIndex[lambda][l] << std::endl;
#endif
  return const_cast<const bool**>(arrayPointer_C[lambda][pathIndexToArrayIndex[lambda][l]]);
}

double ** ListDecoder::getArrayPointer_P(int32_t lambda, int32_t l) {
#ifdef LOG_FLOW
  std::cout << "getArrayPointer_P " << lambda << " " << pathIndexToArrayIndex[lambda][l] << std::endl;
#endif
  int32_t s = pathIndexToArrayIndex[lambda][l];
  int32_t s1 = 0;
  if (arrayReferenceCount[lambda][s] == 1) {
    s1 = s;
  } else {
    s1 = inactiveArrayIndeces[lambda].top();
    inactiveArrayIndeces[lambda].pop();
    for (int i = 0; i < (1 << (m - lambda)); ++i) {
      memcpy(arrayPointer_P[lambda][s1][i], arrayPointer_P[lambda][s][i], sizeof(double)*2);
    }
    --arrayReferenceCount[lambda][s];
    arrayReferenceCount[lambda][s1] = 1;
    pathIndexToArrayIndex[lambda][l] = s1;
  }
  return arrayPointer_P[lambda][s1];
}

bool ** ListDecoder::getArrayPointer_C(int32_t lambda, int32_t l) {
#ifdef LOG_FLOW
  std::cout << "getArrayPointer_C " << lambda << " " << pathIndexToArrayIndex[lambda][l] << std::endl;
#endif
  int32_t s = pathIndexToArrayIndex[lambda][l];
  int32_t s1 = 0;
  if (arrayReferenceCount[lambda][s] == 1) {
    s1 = s;
  } else {
    s1 = inactiveArrayIndeces[lambda].top();
    inactiveArrayIndeces[lambda].pop();
    for (int32_t i = 0; i < (1 << (m - lambda)); ++i) {
      memcpy(arrayPointer_C[lambda][s1][i], arrayPointer_C[lambda][s][i], sizeof(bool)*2);
    }
    --arrayReferenceCount[lambda][s];
    arrayReferenceCount[lambda][s1] = 1;
    pathIndexToArrayIndex[lambda][l] = s1;
  }
  return arrayPointer_C[lambda][s1];
}

void ListDecoder::populateContForks(bool **contForks, double **probForks, int32_t ro) {
  std::vector<std::pair<int32_t, double> > pairs(2*L);
  for (int32_t l = 0; l < L; ++l) {
    pairs[2*l].first = 2*l;
    pairs[2*l].second = probForks[l][0];
    pairs[2*l+1].first = 2*l+1;
    pairs[2*l+1].second = probForks[l][1];
  }
  std::sort(pairs.begin(), pairs.end(), 
	    [](const std::pair<int32_t, double> &a, const std::pair<int32_t, double> &b)
	    { return a.second > b.second; });
  for (int32_t i = 0; i < ro; ++i) {
    contForks[pairs[i].first/2][pairs[i].first%2] = true;
  }
}

int32_t ListDecoder::populateProbForks(double **probForks) {
  // populate probForks
  int32_t i = 0;
  for (int32_t l = 0; l < L; ++l) {
    if (pathIndexInactive(l)) {
      probForks[l][0] = -1;
      probForks[l][1] = -1;
    } else {
      const double **Pm = readArrayPointer_P(m, l);
      probForks[l][0] = Pm[0][0];
      probForks[l][1] = Pm[0][1];
      ++i;
    }
  }
  return i;
}
