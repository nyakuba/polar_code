#include "include/channel.h"

void sendAWGN(bool *v, double *y, int32_t n, std::default_random_engine &gen, std::normal_distribution<double> &dist) {
  double *py = y;
  for (int i = 0; i < n; ++i) {
    if (v[i]) {
      *py = 1 + dist(gen);
    } else {
      *py = -1 + dist(gen);
    }
    ++py;
  }
}
