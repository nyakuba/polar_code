#ifndef _STACK_H_
#define _STACK_H_

// #define STACK_LOG

template<class T>
class Stack {
 private:
#ifdef STACK_LOG
    static int32_t sid;
#endif
    int32_t id;
    int32_t _capacity;
    int32_t _size;
    T* _top;
    T* _values;

 public:
    Stack(const Stack<T> & stack);
    Stack(int32_t capacity);
    ~Stack();

    int32_t size() const;
    int32_t capacity() const;
    void push(T & value);
    T & top() const;
    void pop();
    Stack<T> & operator=(const Stack<T> & stack);
    void clear();
};

#include "stack.hpp"

#endif /* _STACK_H_ */
