#ifndef _LIST_DECODER_H_
#define _LIST_DECODER_H_

#include <vector>
#include <stack>
#include "common.h"
#include "stack.h"

class ListDecoder {
 private:
  const int32_t n, k, m, L;
  bool *frozen;
  bool *activePath;
  bool ****arrayPointer_C;
  int32_t *indeces;
  int32_t **arrayReferenceCount;
  int32_t **pathIndexToArrayIndex;
  double stddev;
  double ****arrayPointer_P;
  Stack<int32_t> inactivePathIndeces;
  std::vector<Stack<int32_t> > inactiveArrayIndeces;
  /* std::stack<int32_t, std::vector<int32_t> > inactivePathIndeces; */
  /* std::stack<int32_t, std::vector<int32_t> > *inactiveArrayIndeces; */

 public:
  ListDecoder(int32_t n, int32_t k, int32_t L, int32_t *indeces, double stddev);
  ~ListDecoder();
 
  ///@param received vector to decode
  ///@return decoded codeword
  void decode(double *y, bool *c);
 
 private:
  inline double W(const double &y, const double &x) const {
    return 1 / (1 + exp((-1)*x*2 * y / (stddev*stddev)));
  }
  inline bool pathIndexInactive(const int32_t &l) const {
    return !activePath[l];
  }

  ///@return index l of initial path
  int32_t assignInitialPath();

  ///@param index l of path to clone
  ///@return index l' to copy
  int32_t clonePath(int32_t l);

  ///@param index l of path to kill
  void killPath(int32_t l);

  // returns private copy of ArrayPointer_P if ReferenceCount > 1
  ///@param layer lambda
  ///@param path index l
  ///@return pointer to corresponding probability pair array
  double **getArrayPointer_P(int32_t lambda, int32_t l);

  // returns private copy of ArrayPointer_C if ReferenceCount > 1
  ///@param layer lambda
  ///@param path index l
  ///@return pointer to corresponding bit pair array
  bool **getArrayPointer_C(int32_t lambda, int32_t l);

  // no private copy
  ///@param layer lambda
  ///@param path index l
  ///@return pointer to corresponding bit pair array
  const double **readArrayPointer_P(int32_t lambda, int32_t l);

  // no private copy
  ///@param layer lambda
  ///@param path index l
  ///@return pointer to corresponding bit pair array
  const bool **readArrayPointer_C(int32_t lambda, int32_t l);

  ///@param layer lambda
  ///@param phase fi
  void recursivelyCalcP(int32_t lambda, int32_t fi);

  ///@param layer lambda
  ///@param phase fi
  void recursivelyUpdateC(int32_t lambda, int32_t fi);

  ///@param phase fi
  void continuePath_FrozenBit(int32_t fi);

  ///@param probabilities arrays
  ///@return i
  int32_t populateProbForks(double **probForks);

  // Can be done in O(L) time. (qsort modification)
  // Now implemented with std::sort
  ///@param contForks[l][b] = true iff probForks[l][b] is one of the largest entries in probForks
  void populateContForks(bool **contForks, double **probForks, int32_t ro);

  ///@param phase fi
  void continuePath_UnfrozenBit(int32_t fi);

  ///@return index of the most probable path
  int32_t findMostProbablePath();
};

#endif /* _LIST_DECODER_H_ */
