#include <math.h>
#include <algorithm>

double ChungPhi(double x);
double ChungPhiInverse(double y);
double XORDensity(double SrcLLRMean);
double SumDensity(double SrcLLRMean);
void GetPolarChannelNoise(unsigned m,//log_2(code length)
	double sigma,//AWGN standard deviation
	double* pNoiseStdDev ///noise standard deviation for each subchannel
	);
void GetSubchannelQuality(unsigned m,//log_2(code length)
	double sigma,//AWGN standard deviation
	double* pChannelErrorProb ///estimated error probability
	);