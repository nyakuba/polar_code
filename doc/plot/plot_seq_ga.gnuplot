set terminal png
set title ""
set output "plot_seq_ga.png"
set xlabel "SNR, db"
set ylabel "Error probability"
set logscale y
set grid xtics ytics mytics
set mytics 10
#set xrange [1:3]
#set yrange [0.0001:1]
set style line 5 lt 1 lw 1 lc rgb "red" pi -1 pt 7 ps 1.5
plot "plot_seq_gaussian_approximation" using 1:2 title "" w lp ls 5