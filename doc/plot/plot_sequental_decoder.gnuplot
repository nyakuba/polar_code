set terminal png
set title "Sequential decoder of polar code (2048,1024),L=32,Theta=2048, opt. for SNR=2."
set output "plot_sequential_decoder.png"
set xlabel "SNR, db"
set ylabel "Error probability"
set logscale y
set grid xtics ytics mytics
set mytics 10
set style line 5 lt 1 lw 1 lc rgb "red" pi -1 pt 7 ps 1.5
set style line 6 lt 1 lw 1 lc rgb "blue" pi -1 pt 7 ps 1.5
set multiplot
plot "plot_sequential_decoder" using 1:2 title "Sequential decoder (2048,1024), L=32, Theta=2048" w lp ls 5, \
     "plot_list_decoder" using 1:2 title "List decoder (2048,1024), L=32" w lp ls 6
unset multiplot