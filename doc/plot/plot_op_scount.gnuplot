set terminal png
set title "Comparison and addition operations count"
set output "plot_ops_scount.png"
set xlabel "SNR, db"
set ylabel "Operations count"
set grid xtics ytics mytics
set mytics 10
set style line 5 lt 1 lw 1 lc rgb "red" pi -1 pt 7 ps 1.5
set style line 6 lt 1 lw 1 lc rgb "blue" pi -1 pt 7 ps 1.5
set style line 7 lt 1 lw 1 lc rgb "green" pi -1 pt 7 ps 1.5
set style line 8 lt 1 lw 1 lc rgb "yellow" pi -1 pt 7 ps 1.5
set multiplot
plot "plot_op_scount" using 1:2 title "fib heap cmp ops" w lp ls 5, \
     "plot_op_scount" using 1:3 title "mm heap cmp ops" w lp ls 6, \
     "plot_op_scount" using 1:4 title "mbucket heap cmp ops" w lp ls 7, \
     "plot_op_scount" using 1:5 title "add ops" w lp ls 8
unset multiplot