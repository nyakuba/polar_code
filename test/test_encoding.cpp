#include <iostream>
#include "../include/common.h"
#include "../include/encoding.h"

void test_encoding1() {
  std::cout << "start test encoding" << std::endl;
  bool u[7] = {1,1,1,1,1,1,1};
  bool v[16] = {0};
  bool v1[16] = {0};
  int32_t shuffle[16] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
  int32_t work[16] = {0}, work1[16] = {0};
  bool work2[16] = {0};
  int32_t indeces[16] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
  double z[16] = {0};
  initZ_BEC(z, 16, 0.5);
  println_seq(z, 16);
  sortZ(z, indeces, 16, 0, 16);
  std::cout << "indeces" << std::endl;
  println_seq(indeces, 16);
  reverse_shuffle(shuffle, 16, work, work1);
  std::cout << "shuffle" << std::endl;
  println_seq(shuffle, 16);
  
  std::cout << "encoding" << std::endl;
  println_seq(u, 7);

  for (int i = 0; i < 7; ++i) {
    work2[indeces[i]] = u[i];
  }
  println_seq(work2, 16, true);

  memset(work, 0, sizeof(int32_t)*16);
  encode(work2, v, 16, shuffle);
  println_seq(v, 16, true);

  memset(work, 0, sizeof(int32_t)*16);
  encode(v, v1, 16, shuffle);
  println_seq(v1, 16, true);

  std::cout << "end test encoding" << std::endl;
}

void test_initZ_BEC() {
  double z[16];
  initZ_BEC(z, 16, 0.5);
  println_seq(z, 16);
}

void test_sort() {
  double z[16];
  int32_t indeces[16] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
  initZ_BEC(z, 16, 0.5);
  sortZ(z, indeces, 16, 0, 16);
  println_seq(z, 16);
  println_seq(indeces, 16);
}

void test_sort1() {
  double z[1] = {5};
  int32_t indeces[1] = {1};
  sortZ(z, indeces, 1, 0, 1);
  println_seq(z, 1);
  println_seq(indeces, 1);
}

void test_sort2() {
  double z[10] = {9,8,7,6,5,4,3,2,1,0};
  int32_t indeces[10] = {0,1,2,3,4,5,6,7,8,9};
  sortZ(z, indeces, 10, 0, 10);
  println_seq(z, 10);
  println_seq(indeces, 10);
}

void test_encoding() {

  test_initZ_BEC();
  test_sort();
  test_sort1();
  test_sort2();

}
