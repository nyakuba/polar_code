#include <iostream>
#include "../include/common.h"
#include "../include/code.h"
#include "../include/encoding.h"

void test_println() { 
  int32_t seq1[10] = {555, 666, 111, 222, 123, 5, 0, 1};
  println_seq(seq1, 10);
}

void test_reverse_shuffle_small() {
  int32_t seq[4] = {0, 1, 2, 3};
  int32_t tmp[4], work[4];
  int32_t exp[4] = {0, 2, 1, 3};
  reverse_shuffle(seq, 4, tmp, work);
  bool equals = true;
  for (int i = 0; i < 4; ++i) {
    if (seq[i] != exp[i]) {
      equals = false;
      break;
    }
  }
  if (!equals) {
    std::cout << "fail test_reverse_shuffle_small" << std::endl;
    println_seq(seq, 4);
    println_seq(exp, 4);
  }
}

void test_reverse_shuffle() {
  int32_t seq[16] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
  int32_t tmp[16], work[16];
  int32_t exp[16] = {0,8,4,12,2,10,6,14,1,9,5,13,3,11,7,15};
  reverse_shuffle(seq, 16, tmp, work);
  bool equals = true;
  for (int i = 0; i < 16; ++i) {
    if (seq[i] != exp[i]) {
      equals = false;
      break;
    }
  }
  if (!equals) {
    std::cout << "fail test_reverse_shuffle" << std::endl;
    println_seq(seq, 16);
    println_seq(exp, 16);
  }
}

void test_code() {
  
  test_println();
  test_reverse_shuffle_small();
  test_reverse_shuffle();

}
